<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/**
 * Testimonials Shortcode
 */

$args = get_query_var('like_sc_blog');

$query_args = array(
	'post_type' => 'post',
	'post_status' => 'publish',
	'posts_per_page' => 3,
);

if ( !empty($args['ids']) ) $query_args['post__in'] = explode(',', esc_attr($args['ids']));
	else
if ( !empty($args['cat']) ) $query_args['category__and'] = esc_attr($args['cat']);

$query = new WP_Query( $query_args );

if ( $query->have_posts() ) {

	$class = 'layout-'.esc_attr($atts['layout']);

	$post_classes = array();
	$post_classes[] = '';

	$post_classes[] = 'ltx-featured-post';
	$post_classes[] = 'matchHeight';

	if ( $args['layout'] == 'text' ) {

		$args['thumb'] = 'hidden';
		$post_classes[] = 'no-thumb';
	}

	set_query_var( 'chaitan_featured_disabled', true );


	echo '<div class="blog blog-sc row '.esc_attr($class).'">';

	$x = 0;
	while ( $query->have_posts() ):

		$query->the_post();	
		$x++;
?>

	<div class="col-lg-4 col-md-6 col-sm-6<?php if ($x == 3) echo ' hidden-sm hidden-md'; ?>">
		<?php get_template_part( 'tmpl/post-formats/list' ); ?>
	</div>
<?php
	endwhile;

	echo '</div>';

	set_query_var( 'chaitan_featured_disabled', false );
	wp_reset_postdata();
}

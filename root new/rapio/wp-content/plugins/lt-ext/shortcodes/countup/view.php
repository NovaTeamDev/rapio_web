<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/**
 * CountUp Shortcode
 */

$args = get_query_var('like_sc_countup');

$class = '';
if ( !empty($args['class']) ) $class .= ' '. esc_attr($args['class']);
if ( !empty($args['id']) ) $id = ' id="'. esc_attr($args['id']). '"'; else $id = '';

$class .= ' layout-'.esc_attr($args['type']);

echo '<div class="ltx-countup '.esc_attr($class).'"'.$id.'>';
	echo '<div class="row">';

		if ( sizeof($atts['list']) == 6 ) $div_class = ' col-md-2 ';
			else
		if ( sizeof($atts['list']) == 4 ) $div_class = ' col-md-3 ';
			else
		if ( sizeof($atts['list']) == 3 ) $div_class = ' col-md-4 ';

		foreach ( $atts['list'] as $k => $item ) {

			$item['header'] = str_replace(array('{{', '}}'), array('<span>', '</span>'), $item['header']);
			if ( !empty($item['prefix']) ) $prefix = $item['prefix']; else $prefix = '';
			if ( !empty($item['postfix']) ) $postfix = $item['postfix']; else $postfix = '';

			if ( $args['animation'] == 'default' ) $item_class = 'countUp'; else $item_class = '';

			echo '
				<div class="'.esc_attr($div_class).' col-sm-6 col-ms-6 col-xs-12 center-flex countUp-wrap">
					<div class=" countUp-item item">
						<span class="h2">'.esc_html($prefix).'<span class="'.esc_attr($item_class).'" id="'.esc_attr( $args['id'].'-'.$k ).'">'.esc_html($item['number']).'</span>'.esc_html($postfix).'</span>
						<h6 class="subheader">'.wp_kses_post($item['header']).'</h6>';
						if ( !empty($item['descr']) ) echo '<div class="descr">'.wp_kses_post($item['descr']).'</div>';
					echo '</div>
				</div>';
		}
	echo '</div>';
echo '</div>';


<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/*
Plugin Name: LT-Ext
Description: Requied plugin for chaitan WordPress Theme
Version: 1.9.7
Author: Like-Themes
Email: support@like-themes.com
License: GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.txt
*/

require_once 'config.php';

require_once 'inc/functions.php';

require_once 'shortcodes/shortcodes.php';

require_once 'post_types/post_types.php';


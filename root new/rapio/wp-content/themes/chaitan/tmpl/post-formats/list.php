<?php
/**
 * The default template for displaying standard post format
 */

$post_class = '';
$featured = get_query_var( 'chaitan_featured_disabled' );
if ( function_exists( 'FW' ) AND empty ( $featured ) ) {

	$featured_post = fw_get_db_post_option(get_The_ID(), 'featured');
	if ( !empty($featured_post) ) {

		$post_class = 'ltx-featured-post';
	}
}

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( esc_attr($post_class) ); ?>>
	<?php 
		if ( has_post_thumbnail() ) {

			$chaitan_photo_class = 'photo';
        	$chaitan_layout = get_query_var( 'chaitan_layout' );

			$chaitan_image_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_The_ID()), 'full' );
			if ($chaitan_image_src[2] > $chaitan_image_src[1]) $chaitan_photo_class .= ' vertical';
			
		    echo '<a href="'.esc_url(get_the_permalink()).'" class="'.esc_attr($chaitan_photo_class).'">';

        	if ( !empty($featured_post) ) {
			 
			    the_post_thumbnail();	    

			}
				else {

			    the_post_thumbnail();
			}

		    echo '</a>';

		    if ( !empty($featured_post) ) {

		    	chaitan_get_the_cats_archive();
		    }
		}
	?>
    <div class="description">
		<?php

			if ( !empty($featured_post) ) {

				echo '<a href="'. esc_url( get_the_permalink() ) .'" class="date">'.get_the_date().'</a>';
			}
				else {

				chaitan_get_the_cats_archive();
			}
		?>    
        <a href="<?php esc_url( the_permalink() ); ?>" class="header"><h4><?php the_title(); ?></h4></a>
        <div class="text text-page">
			<?php
				add_filter( 'the_content', 'chaitan_excerpt' );
			    if( strpos( $post->post_content, '<!--more-->' ) ) {

			        the_content( esc_html__( 'Read more', 'chaitan' ) );
			    }
			    	else
			    if ( !has_post_thumbnail() OR empty($featured_post) ) {

			    	if ( get_post_format() != 'image' ) {

			    		the_excerpt();
			    	}
			    }	
			?>
        </div>   
       	<?php if ( empty($featured_post) ): ?> 
		<div class="blog-info">
			<?php
           		chaitan_the_post_info();
			?>	
	    </div>            
		<?php endif; ?>
    </div>    
</article>

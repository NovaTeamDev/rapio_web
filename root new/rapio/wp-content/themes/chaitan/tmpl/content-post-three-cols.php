<?php
/**
 * The default template for displaying content 3-cols layout
 *
 * Used for both single and index/archive/search.
 */

?>
<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 item">
	<?php get_template_part( 'tmpl/post-formats/list', get_post_format() ); ?>
</div>

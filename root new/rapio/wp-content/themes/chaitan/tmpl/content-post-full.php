<?php
/**
 * Full blog post
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		if ( has_post_thumbnail() ) {

			echo '<div class="image">';
				
				the_post_thumbnail( 'chaitan-post' );

			echo '</div>';
		}
	?>
    <div class="blog-info blog-info-post-top">
		<?php
            echo '<ul class="blog-info-left">';

            echo '<li class="user">'.get_avatar( get_the_author_meta('user_email'), 50 ).'<span class="info">'. esc_html__( 'by', 'chaitan' ) . ' ' .get_the_author_link().'</span></li>';

			if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && chaitan_categorized_blog() && sizeof(get_the_category()) <= 3) {

				echo '<li>';
					chaitan_get_the_cats_archive();
				echo '</li>';
			}
		    
            echo '</ul>';

            chaitan_the_post_info();
        ?>
    </div>
    <div class="description">
        <div class="text text-page">
			<?php
				the_content();
				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'chaitan' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			?>
        </div>
    </div>	    
    <div class="clearfix"></div>
    <div class="blog-info-post-bottom">
		<?php

			if (has_tag() OR chaitan_plugin_is_active( 'simple-share-buttons-adder' )) {

				echo '<div class="tags-line">';

					if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && chaitan_categorized_blog() && sizeof(get_the_category()) > 2) {

						echo '<div class="cats-many">';
						echo get_the_category_list( esc_html_x( ' / ', 'Used between list items, there is a space after the comma.', 'chaitan' ) );
						echo '</div>';
					}

					if ( has_tag() ) {

						echo '<span class="tags">';
							echo '<span class="header">' . esc_html__( 'Tags:', 'chaitan' ) . '</span>';
							the_tags( '<span class="tags-short">', '', '</span>' );
						echo '</span>';
					}

					if ( chaitan_plugin_is_active( 'simple-share-buttons-adder' ) ) {

						echo do_shortcode( '[ssba-buttons]' );
					}					

				echo '</div>';
			}
		?>	
    </div>	
    <?php 

		chaitan_author_bio();

		chaitan_related_posts(get_the_ID());

    ?>
</article>

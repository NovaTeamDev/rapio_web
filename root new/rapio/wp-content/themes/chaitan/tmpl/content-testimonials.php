<?php
/**
	Testimonials Single Item
 */

if ( function_exists( 'FW' ) ) $subheader = fw_get_db_post_option(get_The_ID(), 'subheader');

?>
<div class="col-lg-6">
	<article class="inner matchHeight">
		<div class="top">
			<?php the_post_thumbnail('chaitan-client'); ?>
			<div class="name font-headers color-black"><?php the_title(); ?></div>
			<?php if (!empty($subheader)) { echo '<div class="subheader color-main font-main">'. wp_kses_post($subheader) .'</div>'; } ?>
		</div>
		<div class="text"><?php the_content() ?></div>	
		<span class="quote fa fa-quote-left"></span>
	</article>
</div>

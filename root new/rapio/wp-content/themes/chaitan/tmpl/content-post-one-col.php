<?php
/**
 * The default template for displaying content classic layout
 *
 * Used for both single and index/archive/search.
 */

?>
<div class="col-lg-12 col-xs-12">
	<?php get_template_part( 'tmpl/post-formats/list', get_post_format() ); ?>
</div>
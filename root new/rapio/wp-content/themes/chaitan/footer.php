<?php
/**
 * The template for displaying the footer
 */
?>
    </div>
<?php
    /**
     * Footer subscribe block
     */
    chaitan_the_subscribe_block();

    /**
     * Footer Logo
     */
    chaitan_the_footer_logo_section();

    /**
     * Footer widgets area
     */
    chaitan_the_footer_widgets();

    /**
     * Footer copyright
     */
    chaitan_the_copyrights_section();
   
    /**
     * WordPress Core Function
     */   
    wp_footer();
?>
</body>
</html>

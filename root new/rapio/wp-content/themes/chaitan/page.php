<?php
/**
 * The template for displaying all text pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 */

$sidebar_layout = 'right';
$margin_layout = 'margin-default';
if ( function_exists( 'fw_get_db_settings_option' ) ) {

	$sidebar_layout = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'sidebar-layout' );
	$margin_layout = 'margin-'.fw_get_db_post_option( $wp_query->get_queried_object_id(), 'margin-layout' );

	$chaitan_current_scheme_db = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'color-scheme' );
	
	add_filter ('chaitan_current_scheme', function() {
		global $chaitan_current_scheme_db;
		if ($chaitan_current_scheme_db == 'default') $chaitan_current_scheme_db = 1; 
		return $chaitan_current_scheme_db; 
	} );

}

if ( empty($margin_layout) OR $margin_layout == 'margin-' ) $margin_layout = 'margin-default';

get_header(); ?>

	<!-- Content -->
	<div class="inner-page text-page <?php echo esc_attr( $margin_layout ); ?>">
        <div class="row">
        	<?php if ( $sidebar_layout == 'left' ): ?>
        		<?php get_sidebar(); ?>
			<?php endif; ?>
            <div class="<?php if ( empty($sidebar_layout) OR $sidebar_layout == 'hidden' OR $sidebar_layout == 'disabled' ): ?> col-md-12<?php else: ?> col-xl-8 col-lg-8 col-md-8<?php endif; ?> text-page">
				<?php
				while ( have_posts() ) : 

					the_post();

					get_template_part( 'tmpl/content', 'page' );

					if ( comments_open() || get_comments_number() ) {
						
						comments_template();
					}
				endwhile;
				?>
            </div>
        	<?php if ( $sidebar_layout == 'right' ): ?>
        		<?php get_sidebar(); ?>
			<?php endif; ?>        
        </div>
	</div>

<?php

get_footer();

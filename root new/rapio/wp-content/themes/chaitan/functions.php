<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * Theme Includes
 */
require_once get_template_directory() . '/inc/init.php';
require_once get_template_directory() . '/inc/theme-config.php';
require_once get_template_directory() . '/inc/tgmpa.php';
require_once get_template_directory() . '/inc/template-parts.php';

/**
 * Includes template part, allowing to pass variables
 */
if ( !function_exists( 'chaitan_get_template_part' ) ) {

	function chaitan_get_template_part( $slug, $name = null, array $chaitan_params = array() ) {

		/* list of allowable includes */
		$allow = array('tmpl/content-ltx-gallery');

		$slug = $slug;
		if ( ! is_null( $name ) ) {

			$slug .= '-' . $name;
		}

		if (in_array($slug, $allow) AND file_exists(get_template_directory() . '/' . $slug . '.php')) {

			include( get_template_directory() . '/' . $slug . '.php' );
		}
	}
}


/**
 * Print H1 header
*/
if ( !function_exists( 'chaitan_the_h1' ) ) {

	function chaitan_the_h1() {

		$title = chaitan_get_the_h1();
		if ( !empty($title) ) echo '<h1>' . esc_html( $title ) . '</h1>';
	}
}

if ( !function_exists( 'chaitan_get_the_h1' ) ) {

	function chaitan_get_the_h1() {

		$title = single_post_title( '', false );
		if ( empty( $title ) ) {

			$title = post_type_archive_title( '', false );
		}

		if ( empty($title) AND is_home() ) {

			$title = esc_html__( 'Home', 'chaitan' );
		}
			else {

			$title = explode( '&#8211;', wp_get_document_title() );
			$title = $title[0];
		}

		return $title;
	}
}

/**
 * Adds custom post type active item in menu
 */
if ( !function_exists( 'chaitan_add_current_nav_class' ) ) {

	function chaitan_add_current_nav_class( $classes, $item ) {

		// Getting the current post details
		global $post, $wp;

		$id = ( isset( $post->ID ) ? get_the_ID() : null );

		if ( isset( $id ) ) {

			// Getting the post type of the current post
			$current_post_type = get_post_type_object( get_post_type( $post->ID ) );
			if (!empty($current_post_type->rewrite['slug'])) {

				$current_post_type_slug = $current_post_type->rewrite['slug'];
			}
				else {

				$current_post_type_slug = '';
			}

			$home_url = parse_url( esc_url( home_url( add_query_arg( array(), $wp->request ) ) ) );
			if (isset($home_url['path'])) {

				$current_url = esc_url( str_replace( '/', '', $home_url['path'] ) );
			}
				else {

				$current_url = esc_url( home_url( '/' ) );
			}

			$menu_slug = strtolower( trim( $item->url ) );

			if ( !empty($current_post_type_slug) && strpos( $menu_slug,$current_post_type_slug ) !== false && $current_url != '#' && $current_url != '' && $current_url === str_replace( '/', '', parse_url( $item->url, PHP_URL_PATH ) ) ) {

				$classes[] = 'current-menu-item';

			}
				else {

				$classes = array_diff( $classes, array( 'current_page_parent' ) );
			}
		}

		if ( get_post_type() != 'post' && $item->object_id == get_site_option( 'page_for_posts' ) ) {

			$classes = array_diff( $classes, array( 'current_page_parent' ) );
		}

		return $classes;
	}
}

add_action( 'nav_menu_css_class', 'chaitan_add_current_nav_class', 10, 2 );


/**
 * Manual excerpt generation
 */
if ( !function_exists( 'chaitan_excerpt' ) ) {
	
	function chaitan_excerpt( $content, $excerpt = 0 ) {
		
		global $post;

		$format = get_post_format($post->ID);

		if ( ! empty( $post->post_content ) &&
			 ! preg_match( '#<!--more-->#', $post->post_content ) &&
			 ! preg_match( '#<!--nextpage-->#', $post->post_content ) &&
			 ! preg_match( '#twitter.com#', $post->post_content ) &&
			 ! preg_match( '#wp-caption#', $post->post_content )
			) {
			$content = chaitan_cut_excerpt( $post->post_content , $excerpt );
		}

		return $content;
	}
}

if ( !function_exists( 'chaitan_cut_excerpt' ) ) {
	
	function chaitan_cut_excerpt( $content = '', $excerpt = 0 ) {

		$cut = false;
		$excerpt_more = apply_filters( 'excerpt_more', ' ...' );
		$content = chaitan_get_content( $content );
		$texts = preg_grep( '#(<[^>]+>)|(<\/[^>]+>)#s', $content, PREG_GREP_INVERT );
		$total_length = count( preg_split( '//u', implode( '', $texts ), - 1, PREG_SPLIT_NO_EMPTY ) );
		if ( function_exists( 'fw' ) ) {

			$excerpt_set = (int) fw_get_db_settings_option( 'excerpt_auto' );

		}
			else {

			$excerpt_set = 0;
		}

		if ( $excerpt_set == 0 ) {

			$excerpt_set = 255;
		}

		$excerpt_length = (int) apply_filters( 'excerpt_length', $excerpt_set );

		foreach ( $texts as $key => $text ) {

			$text = preg_split( '//u', $text, - 1, PREG_SPLIT_NO_EMPTY );
			$text = array_slice( $text, 0, $excerpt_length );
			$excerpt_length = $excerpt_length - count( $text );
			$cut = $key;

			if ( 0 >= $excerpt_length ) {
				$content[ $key ] = $texts[ $key ] = implode( '', $text );
				break;
			}
		}

		if ( false !== $cut ) {

			array_splice( $content, $cut + 1 );
		}

		$content = chaitan_strip_tags( $texts, $cut );

		$content = implode( ' ', $content );

		$content = preg_replace( '/<\/p>/', '', $content );

		if ( $total_length > $excerpt_length ) {

			$content .= $excerpt_more;
		}

		return wp_kses_post( $content, true );
	}
}

/**
 * Cuts text by the number of characters
 */
if ( !function_exists( 'chaitan_cut_text' ) ) {

	function chaitan_cut_text( $text, $cut = 300, $aft = ' ...' ) {
		if ( empty( $text ) ) {
			return null;
		}

		if ( empty($cut) AND function_exists( 'fw' ) ) {
			$cut = (int) fw_get_db_settings_option( 'excerpt_wc_auto' );
		}

		$text = wp_strip_all_tags( $text, true );
		$text = strip_tags( $text );
		$text = preg_replace( "/<p>|<\/p>|<br>|(( *&nbsp; *)|(\s{2,}))|\\r|\\n/", ' ', $text );
		if ( mb_strlen( $text ) > $cut ) {
			$text = mb_substr( $text, 0, $cut, 'UTF-8' );
			return mb_substr( $text, 0, mb_strripos( $text, ' ', 0, 'UTF-8' ), 'UTF-8' ) . $aft;
		} else {
			return $text;
		}
	}
}

if ( !function_exists( 'chaitan_get_content' ) ) {
	
	function chaitan_get_content( $content = '' ) {

		$result = array();
		$content = capital_P_dangit( $content );

		$content = wptexturize( $content );
		$content = convert_smilies( $content );
		$content = wpautop( $content );
		$content = prepend_attachment( $content );
		$content = wp_make_content_images_responsive( $content );
		$content = strip_shortcodes( $content );
		$content = str_replace( ']]>', ']]&gt;', $content );
		$content = str_replace( array( "\r\n", "\r" ), "\n", $content );
		$content = preg_split( '#(<[^>]+>)|(<\/[^>]+>)#s', trim( $content ), - 1, PREG_SPLIT_DELIM_CAPTURE );
		$content = array_diff( $content, array( "\n", '' ) );
		$content = array_values( $content );

		foreach ( $content as $key => $value ) {

			$result[] = str_replace( array( "\r\n", "\r", "\n" ), '', $value );
		}

		return $result;
	}
}

if ( !function_exists( 'chaitan_strip_tags' ) ) {
	
	function chaitan_strip_tags( $texts = array(), $cut = 0 ) {

		if ( ! is_array( $texts ) ) {
			return $texts;
		}

		$clean = array( '<p>' );

		foreach ( $texts as $key => $value ) {
			if ( $key <= $cut ) {
				$clean[] = $value;
			}
		}

		return $clean;
	}
}

/**
 * Return true|false is woocommerce conditions.
 *
 * @param string $tag
 * @param string|array $attr
 *
 * @return bool
 */
if ( !function_exists( 'chaitan_is_wc' ) ) {

	function chaitan_is_wc($tag, $attr='') {

		if( !class_exists( 'woocommerce' ) ) {
			return false;
		}
		switch ($tag) {

			case 'wc_active':
		        return true;
			
		    case 'woocommerce':
		        if( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
		        	return true;
		        }
				break;
		    case 'shop':
		        if( function_exists( 'is_shop' ) && is_shop() ) {
		        	return true;
		       	}
				break;
			case 'product_category':
		        if( function_exists( 'is_product_category' ) && is_product_category($attr) ) {
		        	return true;
		        }
				break;
		    case 'product_tag':
		        if( function_exists( 'is_product_tag' ) && is_product_tag($attr) ) {
		        	return true;
		        }
				break;
		    case 'product':
		    	if( function_exists( 'is_product' ) && is_product() ) {
		    		return true;
		    	}
				break;
		    case 'cart':
		        if( function_exists( 'is_cart' ) && is_cart() ) {
		        	return true;
		        }
				break;
		    case 'checkout':
		        if( function_exists( 'is_checkout' ) && is_checkout() ) {
		        	return true;
		        }
				break;
		    case 'account_page':
		        if( function_exists( 'is_account_page' ) && is_account_page() ) {
		        	return true;
		        }
				break;
		    case 'wc_endpoint_url':
		        if( function_exists( 'is_wc_endpoint_url' ) && is_wc_endpoint_url($attr) ) {
		        	return true;
		        }
				break;
		    case 'ajax':
		        if( function_exists( 'is_ajax' ) && is_ajax() ) {
		        	return true;
		        }
				break;
		}

		return false;
	}
}

/**
 *  Return true if Visual Composer installed
 */
if ( !function_exists('chaitan_is_vc') ) {

    function chaitan_is_vc() {

        if ( class_exists('WPBakeryVisualComposerAbstract') ) {

            return true;
        }
        	else {

	        return false;
       	}
    }
}


/**
 * Checking active status of plugin
 */
if ( !function_exists( 'v' ) ) {
	
	function chaitan_plugin_is_active( $plugin_var, $plugin_dir = null ) {

		if ( empty( $plugin_dir ) ) {

			$plugin_dir = $plugin_var;
		}

		return in_array( $plugin_dir . '/' . $plugin_var . '.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) );
	}
}

/**
 * Adding custom stylesheet to admin
 */
if ( !function_exists( 'chaitan_admin_css' ) ) {
	
	function chaitan_admin_css() {

		wp_enqueue_style( 'chaitan-admin-css', get_template_directory_uri() . '/css/admin.css', false, '1.0.0' );
	}
}
add_action( 'admin_enqueue_scripts', 'chaitan_admin_css' );

/**
 * Return inverted contrast value of color
 */
if ( !function_exists( 'chaitan_rgb_contrast' ) ) {
	
	function chaitan_rgb_contrast($r, $g, $b) {

		if ($r < 128) {

			return array(255,255,255,0.1);
		}
			else {

			return array(255,255,255,1);
		}
	}
}

/**
 * Lightens/darkens a given colour (hex format), returning the altered colour in hex format.7
 * @param str $hex Colour as hexadecimal (with or without hash);
 * @percent float $percent Decimal ( 0.2 = lighten by 20%(), -0.4 = darken by 40%() )
 * @return str Lightened/Darkend colour as hexadecimal (with hash);
 */
if ( !function_exists( 'chaitan_color_change' ) ) {
	
	function chaitan_color_change( $hex, $percent ) {
		
		$hex = preg_replace( '/[^0-9a-f]/i', '', $hex );
		$new_hex = '#';
		
		if ( strlen( $hex ) < 6 ) {

			$hex = $hex[0] + $hex[0] + $hex[1] + $hex[1] + $hex[2] + $hex[2];
		}
		
		for ($i = 0; $i < 3; $i++) {

			$dec = hexdec( substr( $hex, $i*2, 2 ) );
			$dec = min( max( 0, $dec + $dec * $percent ), 255 ); 
			$new_hex .= str_pad( dechex( $dec ) , 2, 0, STR_PAD_LEFT );
		}		
		
		return $new_hex;
	}
}


/**
 * Return footer widget columns number and hidden widgets array
 * @return array();
 */
if ( !function_exists( 'chaitan_get_footer_cols_num' ) ) {

	function chaitan_get_footer_cols_num() {

		global $wp_query;	

		// Footer columns classes, depends on total columns number
	    $footer_tmpl = array(
	    	4	=>	array(
	    			'col-md-3 col-sm-6 col-ms-12',
	    			'col-md-3 col-sm-6 col-ms-12',
	    			'col-md-3 col-sm-6 col-ms-12',
	    			'col-md-3 col-sm-6 col-ms-12',
	    		),
	    	3	=>	array(
	    			'col-md-4 col-sm-12 col-ms-12',
	    			'col-md-4 col-sm-12 col-ms-12',
	    			'col-md-4 col-sm-12 col-ms-12',
	    			'col-md-4 col-sm-12 col-ms-12',
	    		),
	    	2	=>	array(
	    			'col-md-6 col-sm-12',
	    			'col-md-6 col-sm-12',
	    			'col-md-6 col-sm-12',
	    			'col-md-6 col-sm-12',
	    		),
	    	1	=>	array(
	    			'col-md-8 col-md-offset-2 text-align-center ',
	    			'col-md-8 col-md-offset-2 text-align-center ',
	    			'col-md-8 col-md-offset-2 text-align-center ',
	    			'col-md-8 col-md-offset-2 text-align-center ',
	    		),
	    );	

		if ( function_exists( 'FW' ) ) {

			$col_hidden_mobile = $classes = $footer_hide = array();

		    $footer_layout = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'footer-layout' );
		    if ( $footer_layout != 'disabled') {

		    	$footer_cols_num = 0;
		    	for ($x = 1; $x <= 4; $x++) {

		    		$col_hidden = fw_get_db_settings_option( 'footer_' . $x . '_hide' );
		    		if ( !$col_hidden ) {

		    			$footer_cols_num++;
		    		}
		    			else {

						$footer_hide[$x] = true;
	    			}

	              	$hide_mobile = fw_get_db_settings_option( 'footer_' . $x . '_hide_mobile');
	            	if ( $hide_mobile ) {

	            		$col_hidden_mobile[$x] = 'hidden-xs hidden-ms hidden-sm';
	            	}    	
	            		else {

						$col_hidden_mobile[$x] = '';
	           		}
	            			
		    	}

		    	for ($x = 1; $x <= 4; $x++) {

					if ( isset($footer_tmpl[$footer_cols_num][( $x - 1 )]) ) {

		        		$classes[$x] = $footer_tmpl[$footer_cols_num][( $x - 1 )];
		        	}
		        }	
		    }                
		    	else {

		        $footer_cols_num = 0;
		   	}    		

			return array(
				'num'			=>	$footer_cols_num,
				'hidden'		=>	$footer_hide,
				'hidden_mobile'	=>	$col_hidden_mobile,
				'classes'		=>	$classes,
			);
		}
			else {

			return array();
		}
	}
}


/**
 * Get page header layout
 */
if ( !function_exists( 'chaitan_get_pageheader_layout' ) ) {

	function chaitan_get_pageheader_layout() {

		global $wp_query;

		$pageheader_layout = 'default';
		if ( function_exists( 'FW' ) ) {

			$pageheader_layout = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'header-layout' );
		}

		return $pageheader_layout;	
	}
}

/**
 * Get page header class
 */
if ( !function_exists( 'chaitan_get_pageheader_class' ) ) {

	function chaitan_get_pageheader_class() {
		
		$chaitan_header_class = '';
		$chaitan_h1 = chaitan_get_the_h1();

		if ( !empty($chaitan_h1) ) {

			$chaitan_header_class .= ' header-h1 ';
		}

		if ( function_exists('FW') ) {

			$header_fixed = fw_get_db_settings_option( 'header_fixed' );
			if ( $header_fixed == 'fixed' ) {

				$chaitan_header_class .= ' header-parallax ltx-parallax ';
			}
		}

		if ( function_exists( 'bcn_display' ) && !is_front_page() ) {

			$chaitan_header_class .= ' hasBreadcrumbs';
		}

		return $chaitan_header_class;
	}
}


/**
 * Bcn first crumb title
 * Used for external plugin Breadcrumb NavXT
 */
if ( function_exists( 'bcn_display' ) ) {

	add_filter('bcn_breadcrumb_title', function($title, $type, $id) {

		if ($type[0] === 'home') {

			$title = esc_html__('Home', 'chaitan');
		}
		return $title;
	}, 42, 3);
}


/**
 * Checks WC sidebar position
 */
if ( !function_exists( 'chaitan_get_wc_sidebar_pos' ) ) {

	function chaitan_get_wc_sidebar_pos() {

		if ( chaitan_is_wc('product') ) {

			$chaitan_sidebar = false;
		}
			else {

			$chaitan_sidebar = 'left';
		}

		if ( function_exists( 'FW' ) ) {

			if ( chaitan_is_wc('product') ) {

				$chaitan_sidebar = fw_get_db_settings_option( 'shop_post_sidebar' );
			}	
				else {

				$chaitan_sidebar = fw_get_db_settings_option( 'shop_list_sidebar' );
			}

			if ( $chaitan_sidebar == 'hidden' ) {

				$chaitan_sidebar = false;
			}
		}	

		return $chaitan_sidebar;
	}
}

/**
 * Find first http/s in string
 */
if ( !function_exists( 'chaitan_find_http' ) ) {

	function chaitan_find_http( $string ) {

		$reg_exUrl = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";

		if (preg_match($reg_exUrl, $string, $url)) {

			return $url[0];
	    }
	}	
}

$chaitan_current_scheme =  apply_filters ('chaitan_current_scheme', array());



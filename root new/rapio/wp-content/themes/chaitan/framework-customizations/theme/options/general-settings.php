<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$descr = '';

$options = array(
	'general' => array(
		'title'   => esc_html__( 'General', 'chaitan' ),
		'type'    => 'tab',
		'options' => array(
			'general-box' => array(
				'title'   => esc_html__( 'General Settings', 'chaitan' ),
				'type'    => 'box',
				'options' => array(			
					'logo'    => array(
						'label' => esc_html__( 'Logo', 'chaitan' ),
						'type'  => 'upload',
					),
					'logo-center'    => array(
						'label' => esc_html__( 'Logo Center', 'chaitan' ),
						'desc'  => esc_html__( 'For Homepage Navbar', 'chaitan' ),
						'type'  => 'upload',
					),							
					'logo-dark'    => array(
						'label' => esc_html__( 'Logo Dark', 'chaitan' ),
						'type'  => 'upload',
					),					
					'theme-icon'    => array(
						'label' => esc_html__( 'Theme Icon', 'chaitan' ),
						'desc'  => esc_html__( 'Small icon (26x26) used in headers', 'chaitan' ),
						'type'  => 'upload',
					),					
					'page-loader'    => array(
						'type'    => 'multi-picker',
						'picker'       => array(
							'loader' => array(
								'label'   => esc_html__( 'Page Loader', 'chaitan' ),
								'type'    => 'select',
								'choices' => array(
									'disabled' => esc_html__( 'Disabled', 'chaitan' ),
									'image' => esc_html__( 'Image', 'chaitan' ),
									'enabled' => esc_html__( 'Theme Loader', 'chaitan' ),
								),
								'value' => 'enabled'
							)
						),						
						'choices' => array(
							'image' => array(
								'loader_img'    => array(
									'label' => esc_html__( 'Page Loader Image', 'chaitan' ),
									'type'  => 'upload',
								),
							),
						),
						'value' => 'enabled',
					),	
					'wc_zoom'    => array(
						'label' => esc_html__( 'WooCommerce Product Hover Zoom', 'chaitan' ),
						'type'    => 'select',
						'desc'   => esc_html__( 'Enables mouse hover zoom in single product page', 'chaitan' ),
						'choices' => array(
							'disabled'  => esc_html__( 'Disabled', 'chaitan' ),
							'enabled' => esc_html__( 'Enabled', 'chaitan' ),
						),
						'value' => 'disabled',
					),
					'google_api'    => array(
						'label' => esc_html__( 'Google Maps API Key', 'chaitan' ),
						'desc'  => esc_html__( 'Required for contacts page, also used in widget', 'chaitan' ),
						'type'  => 'text',
					),								
				),
			),
		),
	),
	'layout' => array(
		'title'   => esc_html__( 'Posts Layout', 'chaitan' ),
		'type'    => 'tab',
		'options' => array(

			'layout-box' => array(
				'title'   => esc_html__( 'Default settings', 'chaitan' ),
				'type'    => 'box',
				'options' => array(

					'blog_layout'    => array(
						'label' => esc_html__( 'Blog Layout', 'chaitan' ),
						'desc'   => esc_html__( 'Default blog page layout.', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'classic'  => esc_html__( 'One Column', 'chaitan' ),
							'two-cols' => esc_html__( 'Two Columns', 'chaitan' ),
							'three-cols' => esc_html__( 'Three Columns', 'chaitan' ),
						),
						'value' => 'classic',
					),				
					'blog_list_sidebar'    => array(
						'label' => esc_html__( 'Blog List Sidebar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'hidden'  => esc_html__( 'Hidden', 'chaitan' ),
							'left' => esc_html__( 'Left', 'chaitan' ),
							'right' => esc_html__( 'Right', 'chaitan' ),
						),
						'value' => 'right',
					),				
					'blog_post_sidebar'    => array(
						'label' => esc_html__( 'Blog Post Sidebar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'hidden'  => esc_html__( 'Hidden', 'chaitan' ),
							'left' => esc_html__( 'Left', 'chaitan' ),
							'right' => esc_html__( 'Right', 'chaitan' ),
						),
						'value' => 'right',
					),		
					'services_list_layout'    => array(
						'label' => esc_html__( 'Services List Layout', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'classic'  => esc_html__( 'One Column', 'chaitan' ),
							'two-cols' => esc_html__( 'Two Columns', 'chaitan' ),
							'three-cols' => esc_html__( 'Three Columns', 'chaitan' ),
						),
						'value' => 'two-cols',
					),						
					'services_list_sidebar'    => array(
						'label' => esc_html__( 'Services List Sidebar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'hidden'  => esc_html__( 'Hidden', 'chaitan' ),
							'left' => esc_html__( 'Left', 'chaitan' ),
							'right' => esc_html__( 'Right', 'chaitan' ),
						),
						'value' => 'hidden',
					),				
					'services_post_sidebar'    => array(
						'label' => esc_html__( 'Services Post Sidebar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'hidden'  => esc_html__( 'Hidden', 'chaitan' ),
							'left' => esc_html__( 'Left', 'chaitan' ),
							'right' => esc_html__( 'Right', 'chaitan' ),
						),
						'value' => 'hidden',
					),														
					'gallery_layout'    => array(
						'label' => esc_html__( 'Default Gallery Layout', 'chaitan' ),
						'desc'   => esc_html__( 'Default galley page layout.', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'col-2' => esc_html__( 'Two Columns', 'chaitan' ),
							'col-3' => esc_html__( 'Three Columns', 'chaitan' ),
							'col-4' => esc_html__( 'Four Columns', 'chaitan' ),
						),
						'value' => 'col-2',
					),	
					'shop_list_sidebar'    => array(
						'label' => esc_html__( 'WooCommerce List Sidebar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'hidden'  => esc_html__( 'Hidden', 'chaitan' ),
							'left' => esc_html__( 'Left', 'chaitan' ),
							'right' => esc_html__( 'Right', 'chaitan' ),
						),
						'value' => 'left',
					),				
					'shop_post_sidebar'    => array(
						'label' => esc_html__( 'WooCommerce Product Sidebar', 'chaitan' ),
						'desc'   => esc_html__( 'Blog Post Sidebar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'hidden'  => esc_html__( 'Hidden', 'chaitan' ),
							'left' => esc_html__( 'Left', 'chaitan' ),
							'right' => esc_html__( 'Right', 'chaitan' ),
						),
						'value' => 'hidden',
					),							
					'excerpt_auto'    => array(
						'label' => esc_html__( 'Excerpt Blog Size', 'chaitan' ),
						'desc'  => esc_html__( 'Automaticly cuts content for blogs', 'chaitan' ),
						'value'	=> 350,
						'type'  => 'short-text',
					),
					'excerpt_wc_auto'    => array(
						'label' => esc_html__( 'Excerpt WooCommerce Size', 'chaitan' ),
						'desc'  => esc_html__( 'Automaticly cuts description for products', 'chaitan' ),
						'value'	=> 50,
						'type'  => 'short-text',
					),						
				)
			)
		)
	),
	'fonts' => array(
		'title'   => esc_html__( 'Fonts', 'chaitan' ),
		'type'    => 'tab',
		'options' => array(

			'fonts-box' => array(
				'title'   => esc_html__( 'Fonts Settings', 'chaitan' ),
				'type'    => 'box',
				'options' => array(
					'font-text'                => array(
						'label' => __( 'Paragraph (text) Font', 'chaitan' ),
						'type'  => 'typography-v2',
						'desc'	=>	esc_html__( 'Use https://fonts.google.com/ to find font you need', 'chaitan' ),
						'value'      => array(
							'family'    => 'Open Sans',
							'subset'    => 'latin-ext',
							'variation' => '400',
							'size'      => 0,
							'line-height' => 0,
							'letter-spacing' => 0,
							'color'     => '#000'
						),
						'components' => array(
							'family'         => true,
							'size'           => false,
							'line-height'    => false,
							'letter-spacing' => false,
							'color'          => false
						),
					),
					'font-text-weights'    => array(
						'label' => esc_html__( 'Additonal weights', 'chaitan' ),
						'desc'  => esc_html__( 'Coma separates weights, for example: "600,800"', 'chaitan' ),
						'type'  => 'text',
					),											
					'font-headers'                => array(
						'label' => __( 'Headers Font', 'chaitan' ),
						'type'  => 'typography-v2',
						'value'      => array(
							'family'    => 'Playfair Display',
							'subset'    => 'latin-ext',
							'variation' => 'regular',
							'size'      => 0,
							'line-height' => 0,
							'letter-spacing' => 0,
							'color'     => '#000'
						),
						'components' => array(
							'family'         => true,
							'size'           => false,
							'line-height'    => false,
							'letter-spacing' => false,
							'color'          => false
						),
					),
					'font-headers-weights'    => array(
						'label' => esc_html__( 'Additonal weights', 'chaitan' ),
						'desc'  => esc_html__( 'Coma separates weights, for example: "600,800"', 'chaitan' ),
						'type'  => 'text',
						'value'  => '400i,700,900',						
					),					
				),
			),
		),
	),	
	'header' => array(
		'title'   => esc_html__( 'Header', 'chaitan' ),
		'type'    => 'tab',
		'options' => array(

			'header-box' => array(
				'title'   => esc_html__( 'Header Settings', 'chaitan' ),
				'type'    => 'box',
				'options' => array(
					'navbar-default'    => array(
						'label' => esc_html__( 'Navbar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'desktop' => esc_html__( 'Default Transparent', 'chaitan' ),
							'white'  => esc_html__( 'White Static', 'chaitan' ),
						),
						'value' => 'desktop',
					),	
					'navbar-affix'    => array(
						'label' => esc_html__( 'Navbar Sticked', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'' => esc_html__( 'All Static', 'chaitan' ),
							'affix'  => esc_html__( 'Desktop Sticked', 'chaitan' ),
						),
						'value' => '',
					),	

					'basket-icon'    => array(
						'label' => esc_html__( 'Basket icon in navbar', 'chaitan' ),
						'desc'   => esc_html__( 'As replacement for basket in topbar in mobile view', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'disabled' => esc_html__( 'Hidden', 'chaitan' ),
							'mobile'  => esc_html__( 'Visible on Mobile', 'chaitan' ),
						),
						'value' => 'disabled',
					),					

					'header_bg'    => array(
						'label' => esc_html__( 'Inner Header Background', 'chaitan' ),
						'desc'  => esc_html__( 'By default header is gray, you can replace it with background image', 'chaitan' ),
						'type'  => 'upload',
					),  				
					'header_fixed'    => array(
						'label' => esc_html__( 'Background parallax', 'chaitan' ),
						'desc'   => esc_html__( 'Parallax effect requires large images', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'disabled' => esc_html__( 'Disabled', 'chaitan' ),
							'fixed'  => esc_html__( 'Enabled', 'chaitan' ),
						),
						'value' => 'fixed',
					),
					'header_position'    => array(
						'label' => esc_html__( 'Background images position X Y', 'chaitan' ),
						'desc'   => esc_html__( 'For example "50% 50%"', 'chaitan' ),
						'type'    => 'text',
						'value' => '50% -50%',
					),										        
					'featured_bg'    => array(
						'label' => esc_html__( 'Featured Images as Background', 'chaitan' ),
						'desc'  => esc_html__( 'Use Featured Image for Page as Header Background', 'chaitan' ),
						'type'    => 'select',						
						'choices' => array(
							'disabled'  => esc_html__( 'Disabled', 'chaitan' ),
							'enabled' => esc_html__( 'Enabled', 'chaitan' ),
						),
						'value' => 'disabled',
					),	
					'header_overlay'    => array(
						'label' => esc_html__( 'Inner Header Overlay', 'chaitan' ),
						'desc'  => esc_html__( 'Image will be placed over pages header', 'chaitan' ),
						'type'  => 'upload',
					),
					'header_position_overlay'    => array(
						'label' => esc_html__( 'Overlay image position X Y', 'chaitan' ),
						'desc'   => esc_html__( 'For example "50% 50%"', 'chaitan' ),
						'type'    => 'text',
						'value' => '100% -20px',
					),						
					'navbar-icons' => array(
		                'label' => esc_html__( 'Topbar Icons', 'chaitan' ),
		                'desc' => esc_html__( 'Visible at navbar area', 'chaitan' ),
		                'type' => 'addable-box',
		                'value' => array(),
		                'box-options' => array(
							'type'        => array(
								'type'         => 'multi-picker',
								'label'        => false,
								'desc'         => false,
								'picker'       => array(
									'type_radio' => array(
										'label'   => esc_html__( 'Type', 'chaitan' ),
										'type'    => 'radio',
										'choices' => array(
											'search' => esc_html__( 'Search', 'chaitan' ),
											'basket'  => esc_html__( 'WooCommerce Cart', 'chaitan' ),
											'profile'  => esc_html__( 'User Profile', 'chaitan' ),
											'social'  => esc_html__( 'Social Icon', 'chaitan' ),
										),
									)
								),
								'choices'      => array(
									'basket'  => array(
										'count'    => array(
											'label' => esc_html__( 'Count', 'chaitan' ),
											'type'    => 'select',
											'choices' => array(
												'show' => esc_html__( 'Show count label', 'chaitan' ),
												'hide'  => esc_html__( 'Hide count label', 'chaitan' ),
											),
											'value' => 'show',
										),											
									),
									'profile'  => array(
					                    'header' => array(
					                        'label' => esc_html__( 'Non-logged header', 'chaitan' ),
					                        'type' => 'text',
					                        'value' => '',
					                    ),										
									),
									'social'  => array(
					                    'text' => array(
					                        'label' => esc_html__( 'Label', 'chaitan' ),
					                        'type' => 'text',
					                    ),
					                    'href' => array(
					                        'label' => esc_html__( 'External Link', 'chaitan' ),
					                        'type' => 'text',
					                        'value' => '#',
					                    ),											
									),		
								),
								'show_borders' => false,
							),	  														                	
							'icon-type'        => array(
								'type'         => 'multi-picker',
								'label'        => false,
								'desc'         => false,
								'value'        => array(
									'icon_radio' => 'default',
								),
								'picker'       => array(
									'icon_radio' => array(
										'label'   => esc_html__( 'Icon', 'chaitan' ),
										'type'    => 'radio',
										'choices' => array(
											'default'  => esc_html__( 'Default', 'chaitan' ),
											'fa' => esc_html__( 'FontAwesome', 'chaitan' )
										),
										'desc'    => esc_html__( 'For social icons you need to use FontAwesome in any case.',
											'chaitan' ),
									)
								),
								'choices'      => array(
									'default'  => array(
									),
									'fa' => array(
										'icon_v2'  => array(
											'type'  => 'icon-v2',
											'label' => esc_html__( 'Select Icon', 'chaitan' ),
										),										
									),
								),
								'show_borders' => false,
							),
							'icon-visible'        => array(
								'label'   => esc_html__( 'Visibility', 'chaitan' ),
								'type'    => 'radio',
								'value'    => 'hidden-mob',								
								'choices' => array(
									'hidden-mob'  => esc_html__( 'Hidden on mobile', 'chaitan' ),
									'visible-mob' => esc_html__( 'Visible on mobile', 'chaitan' )
								),
							),							
		                ),
                		'template' => '{{- type.type_radio }}',		                
                    ),		
				),
			),
		),
	),	
	'social' => array(
		'title'   => esc_html__( 'Social', 'chaitan' ),
		'type'    => 'tab',
		'options' => array(
			'social-box' => array(
				'title'   => esc_html__( 'Social', 'chaitan' ),
				'type'    => 'box',
				'options' => array(			
					'header-social'    => array(
						'label' => esc_html__( 'Social icons in page header', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'disabled'  => esc_html__( 'Disabled', 'chaitan' ),
							'enabled' => esc_html__( 'Enabled', 'chaitan' ),
						),
						'value' => 'enabled',
					),	
					'header-social-text'    => array(
						'label' => esc_html__( 'Follow us text in header icons', 'chaitan' ),
						'type'    => 'text',
					),	
					'footer-social'    => array(
						'label' => esc_html__( 'Social icons in footer', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'disabled'  => esc_html__( 'Disabled', 'chaitan' ),
							'enabled' => esc_html__( 'Enabled', 'chaitan' ),
						),
						'value' => 'enabled',
					),		
					'target-social'    => array(
						'label' => esc_html__( 'Open social links in', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'self'  => esc_html__( 'Same window', 'chaitan' ),
							'blank' => esc_html__( 'New window', 'chaitan' ),
						),
						'value' => 'self',
					),										
		            'social-icons' => array(
		                'label' => esc_html__( 'Socail Icons', 'chaitan' ),
		                'type' => 'addable-box',
		                'value' => array(),
		                'desc' => esc_html__( 'Visible in inner page header', 'chaitan' ),
		                'box-options' => array(
		                    'icon_v2' => array(
		                        'label' => esc_html__( 'Icon', 'chaitan' ),
		                        'type'  => 'icon-v2',
		                    ),
		                    'text' => array(
		                        'label' => esc_html__( 'Text', 'chaitan' ),
		                        'desc' => esc_html__( 'If needed', 'chaitan' ),
		                        'type' => 'text',
		                    ),
		                    'href' => array(
		                        'label' => esc_html__( 'Link', 'chaitan' ),
		                        'type' => 'text',
		                        'value' => '#',
		                    ),		                    
		                ),
                		'template' => '{{- text }}',		                
                    ),								
				),
			),
		),
	),
	'footer' => array(
		'title'   => esc_html__( 'Footer', 'chaitan' ),
		'type'    => 'tab',
		'options' => array(

			'footer-box' => array(
				'title'   => esc_html__( 'Footer Settings', 'chaitan' ),
				'type'    => 'box',
				'options' => array(	
					'subscribe'    => array(
						'label' => esc_html__( 'Subscribe Block', 'chaitan' ),
						'desc'   => esc_html__( 'You must install MailChimp plugin to use it', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'default' => esc_html__( 'Default page settings', 'chaitan' ),
							'visible'  => esc_html__( 'Always visible', 'chaitan' ),
							'hidden'  => esc_html__( 'Always hidden', 'chaitan' ),
						),
						'value' => 'default',
					),		
					'subscribe_bg'    => array(
						'label' => esc_html__( 'Subscribe Background', 'chaitan' ),
						'type'  => 'upload',
					),						
					'footer_top_layout'    => array(
						'label' => esc_html__( 'Footer Logo Area', 'chaitan' ),
						'desc'   => esc_html__( 'Contains Logo and social icons', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'visible'  => esc_html__( 'Visible', 'chaitan' ),
							'hidden'  => esc_html__( 'Hidden', 'chaitan' ),
						),
						'value' => 'visible',
					),						
					'footer_top_bg'    => array(
						'label' => esc_html__( 'Footer Logo Area Background', 'chaitan' ),
						'type'  => 'upload',
					),						
					'footer_bg'    => array(
						'label' => esc_html__( 'Footer Widgets Background', 'chaitan' ),
						'desc'   => esc_html__( 'Widgets area will be hidden if no widgets activated', 'chaitan' ),						
						'type'  => 'upload',
					),																	
					'footer_1_hide'    => array(
						'label' => esc_html__( 'Hide Footer 1 Widget', 'chaitan' ),
						'type'  => 'checkbox',
						'value'	=> 'true',
					),
					'footer_2_hide'    => array(
						'label' => esc_html__( 'Hide Footer 2 Widgets', 'chaitan' ),
						'type'  => 'checkbox',
						'value'	=> 'true',
					),
					'footer_3_hide'    => array(
						'label' => esc_html__( 'Hide Footer 3 Widgets', 'chaitan' ),
						'type'  => 'checkbox',
						'value'	=> 'true',
					),
					'footer_4_hide'    => array(
						'label' => esc_html__( 'Hide Footer 4 Widgets', 'chaitan' ),
						'type'  => 'checkbox',
						'value'	=> 'true',
					),						
					'footer_1_hide_mobile'    => array(
						'label' => esc_html__( 'Hide Footer 1 Widgets on mobile', 'chaitan' ),
						'type'  => 'switch',
						'value'	=> 'no',
					),
					'footer_2_hide_mobile'    => array(
						'label' => esc_html__( 'Hide Footer 2 Widgets on mobile', 'chaitan' ),
						'type'  => 'switch',
						'value'	=> 'yes',
					),
					'footer_3_hide_mobile'    => array(
						'label' => esc_html__( 'Hide Footer 3 Widgets on mobile', 'chaitan' ),
						'type'  => 'switch',
						'value'	=> 'no',
					),
					'footer_4_hide_mobile'    => array(
						'label' => esc_html__( 'Hide Footer 4 Widgets on mobile', 'chaitan' ),
						'type'  => 'switch',
						'value'	=> 'yes',
					),	
					'go_top_img'    => array(
						'label' => esc_html__( 'Go Top Image', 'chaitan' ),
						'type'  => 'upload',
					),					
					'go_top_hide'    => array(
						'label' => esc_html__( 'Hide Go Top button in footer', 'chaitan' ),
						'type'  => 'switch',
						'value'	=> 'no',
					),			
					'copyrights'    => array(
						'label' => esc_html__( 'Copyrights', 'chaitan' ),
						'type'  => 'wp-editor',
					),									
				),
			),
		),
	),
);


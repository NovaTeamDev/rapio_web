<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$chaitan_choices =  array();
$chaitan_choices['default'] = esc_html__( 'Default', 'chaitan' );
$chaitan_color_schemes = fw_get_db_settings_option( 'items' );
if ( !empty($chaitan_color_schemes) ) {

	foreach ($chaitan_color_schemes as $v) {

		$chaitan_choices[$v['slug']] = esc_html( $v['name'] );
	}
}

$options = array(
	'general' => array(
		'title'   => esc_html__( 'Page settings', 'chaitan' ),
		'type'    => 'box',
		'options' => array(		
			'general-box' => array(
				'title'   => __( 'General Settings', 'chaitan' ),
				'type'    => 'tab',
				'options' => array(

					'margin-layout'    => array(
						'label' => esc_html__( 'Content Margin', 'chaitan' ),
						'type'    => 'select',
						'description'   => esc_html__( 'Margins control for content', 'chaitan' ),
						'choices' => array(
							'default'  => esc_html__( 'Top And Bottom', 'chaitan' ),
							'top'  => esc_html__( 'Top Only', 'chaitan' ),
							'bottom'  => esc_html__( 'Bottom Only', 'chaitan' ),
							'disabled' => esc_html__( 'Margin Removed', 'chaitan' ),
						),
						'value' => 'default',
					),	
					'navbar-layout'    => array(
						'label' => esc_html__( 'Navbar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'desktop'	=> esc_html__( 'Default Transparent', 'chaitan' ),
							'white'	=> esc_html__( 'White Static', 'chaitan' ),
							'desktop-center-absolute'	=> esc_html__( 'Desktop with Center Logo Transparent', 'chaitan' ),
							'disabled'  	=> esc_html__( 'Hidden', 'chaitan' ),
						),
						'value' => 'desktop',
					),			
					'header-layout'    => array(
						'label' => esc_html__( 'Page Header', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'default'  => esc_html__( 'Default', 'chaitan' ),
							'disabled' => esc_html__( 'Hidden', 'chaitan' ),
						),
						'value' => 'default',
					),		
					'topbar-layout'    => array(
						'label' => esc_html__( 'Topbar Block', 'chaitan' ),
						'type'    => 'select',
						'description'   => esc_html__( 'Topbar visibility. Can be edited from Sections Menu.', 'chaitan' ),
						'choices' => array(
							'default'  => esc_html__( 'Default', 'chaitan' ),
							'disabled' => esc_html__( 'Hidden', 'chaitan' ),
						),
						'value' => 'default',
					),								
					'subscribe-layout'    => array(
						'label' => esc_html__( 'Subscribe Block', 'chaitan' ),
						'type'    => 'select',
						'description'   => esc_html__( 'Subscribe block before footer. Can be edited from Sections Menu.', 'chaitan' ),
						'choices' => array(
							'default'  => esc_html__( 'Default', 'chaitan' ),
							'disabled' => esc_html__( 'Hidden', 'chaitan' ),
						),
						'value' => 'default',
					),
					'footer-layout'    => array(
						'label' => esc_html__( 'Footer Block', 'chaitan' ),
						'type'    => 'select',
						'description'   => esc_html__( 'Footer block before footer. Edited in Widgets menu.', 'chaitan' ),
						'choices' => array(
							'default'  => esc_html__( 'Default', 'chaitan' ),
							'disabled' => esc_html__( 'Hidden', 'chaitan' ),
						),
						'value' => 'default',
					),						
				),
			),	
			'cpt' => array(
				'title'   => esc_html__( 'Blog / Gallery', 'chaitan' ),
				'type'    => 'tab',
				'options' => array(				
					'sidebar-layout'    => array(
						'label' => esc_html__( 'Blog Sidebar', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'hidden' => esc_html__( 'Hidden', 'chaitan' ),
							'left'  => esc_html__( 'Sidebar Left', 'chaitan' ),
							'right'  => esc_html__( 'Sidebar Right', 'chaitan' ),
						),
						'value' => 'hidden',
					),						
					'blog-layout'    => array(
						'label' => esc_html__( 'Blog Layout', 'chaitan' ),
						'description'   => esc_html__( 'Used only for blog pages.', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'default'  => esc_html__( 'Default', 'chaitan' ),
							'classic'  => esc_html__( 'One Column', 'chaitan' ),
							'two-cols' => esc_html__( 'Two Columns', 'chaitan' ),
							'three-cols' => esc_html__( 'Three Columns', 'chaitan' ),
						),
						'value' => 'default',
					),
					'gallery-layout'    => array(
						'label' => esc_html__( 'Gallery Layout', 'chaitan' ),
						'description'   => esc_html__( 'Used only for gallery pages.', 'chaitan' ),
						'type'    => 'select',
						'choices' => array(
							'default' => esc_html__( 'Default', 'chaitan' ),
							'col-2' => esc_html__( 'Two Columns', 'chaitan' ),
							'col-3' => esc_html__( 'Three Columns', 'chaitan' ),
							'col-4' => esc_html__( 'Four Columns', 'chaitan' ),
						),
						'value' => 'default',
					),					
				)
			)	
		)
	),
);



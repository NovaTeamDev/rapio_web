<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * Framework options
 *
 * @var array $options Fill this array with options to generate framework settings form in backend
 */
//$cfg['settings_form_side_tabs'] = true;

$options = array(
	fw()->theme->get_options( 'general-settings' ),
);

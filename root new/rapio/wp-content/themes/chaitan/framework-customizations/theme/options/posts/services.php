<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'main' => array(
		'title'   => false,
		'type'    => 'box',
		'options' => array(
			'header'    => array(
				'label' => esc_html__( 'Alternative Header', 'chaitan' ),
				'desc' => esc_html__( 'Use {{ brackets }} to add subheader', 'chaitan' ),
				'type'  => 'text',
			),			
			'link'    => array(
				'label' => esc_html__( 'External Link', 'chaitan' ),
				'type'  => 'text',
			),			
			'cut'    => array(
				'label' => esc_html__( 'Short Description', 'chaitan' ),
				'type'  => 'textarea',
			),
			'icon' => array(
				'label' => esc_html__( 'Icon', 'chaitan' ),
				'type'  => 'upload',
			),			
		),
	),
);


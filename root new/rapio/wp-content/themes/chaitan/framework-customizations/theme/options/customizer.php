<?php if ( ! defined( 'FW' ) ) { die( 'Forbidden' ); }

$options = array(
    'chaitan_customizer' => array(
        'title' => esc_html__('Chaitan settings', 'chaitan'),
        'position' => 1,
        'options' => array(

            'main_color' => array(
                'type' => 'color-picker',
                'value' => '#88B44E',
                'label' => esc_html__('Main Color', 'chaitan'),
            ),
/*
            'second_color' => array(
                'type' => 'color-picker',
                'value' => '#f9dc31',
                'label' => esc_html__('Secondary Color', 'chaitan'),
            ),
*/            
            'gray_color' => array(
                'type' => 'color-picker',
                'value' => '#F5F8F2',
                'label' => esc_html__('Gray Color', 'chaitan'),
            ),
            'white_color' => array(
                'type' => 'color-picker',
                'value' => '#ffffff',
                'label' => esc_html__('White Color', 'chaitan'),
            ),

            'black_color' => array(
                'type' => 'color-picker',
                'value' => '#252C30',
                'label' => esc_html__('Black Color', 'chaitan'),
            ),      
            'nav_opacity' => array(
                'type'  => 'slider',
                'value' => 0,
                'properties' => array(
                    'min' => 0,
                    'max' => 1,
                    'step' => 0.05,
                ),
                'label' => esc_html__('Navbar Opacity (0 - 1)', 'chaitan'),
            ), 
            'nav_opacity_scroll' => array(
                'type'  => 'slider',
                'value' => 0.95,
                'properties' => array(
                    'min' => 0,
                    'max' => 1,
                    'step' => 0.05,
                ),
                'label' => esc_html__('Navbar Sticked Opacity (0 - 1)', 'chaitan'),
            ),
            'logo_height' => array(
                'type'  => 'slider',
                'value' => 75,
                'properties' => array(

                    'min' => 16,
                    'max' => 110,
                    'step' => 1,

                ),
                'label' => esc_html__('Logo Max Height, px', 'chaitan'),
            ),        
        ),
    ),
);

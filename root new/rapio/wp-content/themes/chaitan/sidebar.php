<?php
/**
 * The sidebar containing the main widget area
 *
 */

if ( chaitan_is_wc('woocommerce') || chaitan_is_wc('shop') || chaitan_is_wc('product') ) : ?>
			<?php 
				$chaitan_sidebar = chaitan_get_wc_sidebar_pos();
			?>
			<?php if ( is_active_sidebar( 'sidebar-wc' ) AND !empty( $chaitan_sidebar ) ): ?>
			<?php if ( $chaitan_sidebar == 'left' ): ?>
			<div class="col-xl-3 col-xl-pull-8 col-lg-4 col-lg-pull-8 col-md-12 div-sidebar matchHeight" data-mh="ltx-col">
			<?php else: ?>
			<div class="col-xl-3 col-xl-offset-1 col-lg-4 col-md-12 col-xs-12 div-sidebar matchHeight" data-mh="ltx-col">
			<?php endif; ?>
				<div id="content-sidebar" class="content-sidebar woocommerce-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'sidebar-wc' ); ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
<?php elseif ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div class="col-xl-3 col-xl-offset-1 col-lg-4 col-md-4 col-sm-12 col-xs-12 div-sidebar matchHeight" data-mh="ltx-col">
		<div id="content-sidebar" class="content-sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>
	</div>
<?php endif; ?>

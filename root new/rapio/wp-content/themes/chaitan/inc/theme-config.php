<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Theme Configuration
 */

/*
* Adding additional TinyMCE options
*/
function chaitan_wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'chaitan_wpb_mce_buttons_2');

function chaitan_mce_before_init_insert_formats( $init_array ) {  

    $style_formats = array(

        array(  
            'title' => esc_html__('Main Color', 'chaitan'),
            'block' => 'span',  
            'classes' => 'color-main',
            'wrapper' => true,
        ),  
        array(  
            'title' => esc_html__('Second Color', 'chaitan'),
            'block' => 'span',  
            'classes' => 'color-second',
            'wrapper' => true,
        ),
        array(  
            'title' => esc_html__('White Color', 'chaitan'),
            'block' => 'span',  
            'classes' => 'color-white',
            'wrapper' => true,   
        ),
        array(  
            'title' => esc_html__('Small Text', 'chaitan'),
            'block' => 'span',  
            'classes' => 'text-sm',
            'wrapper' => true,
        ),     
        array(  
            'title' => esc_html__('Medium Text', 'chaitan'),
            'block' => 'span',  
            'classes' => 'text-md',
            'wrapper' => true,
        ),                                
        array(  
            'title' => esc_html__('Large Text', 'chaitan'),
            'block' => 'span',  
            'classes' => 'text-lg',
            'wrapper' => true,
        ),                
        array(  
            'title' => esc_html__('XLarge Text', 'chaitan'),
            'block' => 'span',  
            'classes' => 'text-xlg',
            'wrapper' => true,
        ),   
        array(  
            'title' => esc_html__('Line Before', 'chaitan'),
            'block' => 'span',  
            'classes' => 'p-line-before',
            'wrapper' => true,
        ), 
    );  
    $init_array['style_formats'] = json_encode( $style_formats );  
     
    return $init_array;  
} 
add_filter( 'tiny_mce_before_init', 'chaitan_mce_before_init_insert_formats' ); 


/**
 * Config used for lt-ext plugin to set Visual Composer configuration
 */
add_filter( 'ltx_get_vc_config', 'chaitan_vc_config', 10, 1 );
function chaitan_vc_config( $value ) {

    return array(
    	'sections'	=>	array(
			esc_html__("Displaced floating section", 'chaitan') 		=> "displaced-top",			
			esc_html__("White Top Space", 'chaitan') 		=> "white-space-top",			
			esc_html__("Row with 5 Columns", 'chaitan') 		=> "row-5-cols",
			esc_html__("Hover Logos", 'chaitan') 		=> "ltx-hover-logos",
			esc_html__("Tea Background", 'chaitan') 		=> "ltx-tea-bg",
			esc_html__("Green Cups", 'chaitan') 		=> "ltx-cups",
		),
		'background' => array(
			esc_html__( "Main Color", 'chaitan' ) => "theme_color",
			esc_html__( "Second Color", 'chaitan' ) => "second",			
			esc_html__( "Gray", 'chaitan' ) => "gray",
			esc_html__( "White", 'chaitan' ) => "white",
			esc_html__( "Black", 'chaitan' ) => "black",			
		),
		'overlay'	=> array(
			esc_html__( "Black Overlay", 'chaitan' ) => "black",
			esc_html__( "Dark Overlay", 'chaitan' ) => "dark",
		),
	);
}


/**
 * Register widget areas.
 *
 */
function chaitan_action_theme_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Default', 'chaitan' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Displayed in the right/left section of the site.', 'chaitan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="header-widget">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar WooCommerce', 'chaitan' ),
		'id'            => 'sidebar-wc',
		'description'   => esc_html__( 'Displayed in the right/left section of the site.', 'chaitan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="header-widget">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'chaitan' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Displayed in the footer section of the site.', 'chaitan' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="header-widget">',
		'after_title'   => '</h4>',
	) );			

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'chaitan' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Displayed in the footer section of the site.', 'chaitan' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="header-widget">',
		'after_title'   => '</h4>',
	) );			

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'chaitan' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Displayed in the footer section of the site.', 'chaitan' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="header-widget">',
		'after_title'   => '</h4>',
	) );			

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'chaitan' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Displayed in the footer section of the site.', 'chaitan' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="header-widget">',
		'after_title'   => '</h4>',
	) );			

}

add_action( 'widgets_init', 'chaitan_action_theme_widgets_init' );



/**
 * Additional styles init
 */
function chaitan_css_style() {

	global $wp_query;

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap-grid.css', array(), '1.0' );

	wp_enqueue_style( 'chaitan-plugins-css', get_template_directory_uri() . '/assets/css/plugins.css', array(), '1.0' );

	wp_enqueue_style( 'chaitan-theme-style', get_stylesheet_uri(), array( 'bootstrap', 'chaitan-plugins-css' ), '1.1.0' );

	if ( function_exists( 'fw_get_db_settings_option' ) ){

		$heading_bg = fw_get_db_settings_option( 'heading_bg' );
		if (! empty( $heading_bg ) ) {

			wp_add_inline_style( 'chaitan-theme-style', '.heading.bg-image { background-image: url(' . esc_url( $heading_bg['url'] ) . ') !important; } ' );
		}

		$header_bg = fw_get_db_settings_option( 'header_bg' );

		$featured_bg = fw_get_db_settings_option( 'featured_bg' );
		if (! empty( $header_bg ) ) {

			if ( !is_single() && has_post_thumbnail() && $featured_bg == 'enabled') {

				wp_add_inline_style( 'chaitan-theme-style', '.page-header { background-image: url(' . esc_url( get_the_post_thumbnail_url( $wp_query->get_queried_object_id(), 'full') ) . ') !important; } ' );
			}
				else {

				wp_add_inline_style( 'chaitan-theme-style', '.page-header { background-image: url(' . esc_url( $header_bg['url'] ) . ') !important; } ' );
			}
		}

		$header_overlay = fw_get_db_settings_option( 'header_overlay' );
		$header_overlay_position = fw_get_db_settings_option( 'header_overlay_position' );
		if (! empty( $header_overlay ) ) {

			wp_add_inline_style( 'chaitan-theme-style', '.page-header::after { background-image: url(' . esc_url( $header_overlay['url'] ) . ') !important; } ' );
		}

		$theme_icon = fw_get_db_settings_option( 'theme-icon' );
		if (! empty( $theme_icon ) ) {

			wp_add_inline_style( 'chaitan-theme-style', '.woocommerce-MyAccount-navigation .header-widget, .widget-area .header-widget { background-image: url(' . esc_url( $theme_icon['url'] ) . ') !important; } ' );
		}

		$footer_bg = fw_get_db_settings_option( 'footer_bg' );
		if (! empty( $footer_bg ) ) {

			wp_add_inline_style( 'chaitan-theme-style', '#ltx-widgets-footer { background-image: url(' . esc_url( $footer_bg['url'] ) . ') !important; } ' );
		}

		$footer_top_bg = fw_get_db_settings_option( 'footer_top_bg' );
		if (! empty( $footer_top_bg ) ) {

			wp_add_inline_style( 'chaitan-theme-style', '#ltx-logo-footer { background-image: url(' . esc_url( $footer_top_bg['url'] ) . ') !important; } ' );
		}

		$subscribe_bg = fw_get_db_settings_option( 'subscribe_bg' );
		if (! empty( $subscribe_bg ) ) {

			wp_add_inline_style( 'chaitan-theme-style', '.subscribe-block { background-image: url(' . esc_url( $subscribe_bg['url'] ) . ') !important; } ' );
		}

		$bg_404 = fw_get_db_settings_option( '404_bg' );
		if (! empty( $bg_404 ) ) {

			wp_add_inline_style( 'chaitan-theme-style', 'body.error404 { background-image: url(' . esc_url( $bg_404['url'] ) . ') !important; } ' );
		}

		$go_top_img = fw_get_db_settings_option( 'go_top_img' );
		if (! empty( $go_top_img ) ) {

			wp_add_inline_style( 'chaitan-theme-style', '.go-top:before { background-image: url(' . esc_url( $go_top_img['url'] ) . ') !important; } ' );
		}

		$logo_height = fw_get_db_customizer_option('logo_height');
		if ( !empty($logo_height) ) {

			wp_add_inline_style( 'chaitan-theme-style', 'nav.navbar .logo img { max-height: '.esc_attr($logo_height).'px; } ' );			
		}

		$pace = fw_get_db_settings_option( 'page-loader' );
		if ( !empty($pace) AND !empty($pace['image']) AND !empty($pace['image']['loader_img'])) {

			wp_add_inline_style( 'chaitan-theme-style', '.paceloader-image .pace-image { background-image: url(' . esc_attr( $pace['image']['loader_img']['url'] ) . ') !important; } ' );
		}
		
	}
}
add_action( 'wp_enqueue_scripts', 'chaitan_css_style' );


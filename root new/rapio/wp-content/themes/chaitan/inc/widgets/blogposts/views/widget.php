<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * @var string $before_widget
 * @var string $after_widget
 */
echo wp_kses_post( $before_widget );

if ( !empty($params['title']) )  {

	echo wp_kses_post( $before_title ) . esc_html( $params['title'] ) . wp_kses_post( $after_title );
}

if ( empty($params['num']) ) $params['num'] = 3;

$wp_query = new WP_Query( array(
	'post_type' => 'post',
	'post_status' => 'publish',
	'posts_per_page' => (int)($params['num']),
) );

echo '<div class="items">';

if ( $wp_query->have_posts() ) :
	while ( $wp_query->have_posts() ) : $wp_query->the_post();

		if ( !empty( $params['photo'] ) ) {

			echo '<div class="post">';
				echo '<a href="'.get_the_permalink().'" class="photo">';

	   			the_post_thumbnail('chaitan-tiny');

	   			echo '</a>';

				echo '<div class="blog-info top">';
					echo '<a href="'. esc_url( get_the_permalink() ) .'" class="date">'.get_the_date().'</a>';
					echo '<a href="'.get_the_permalink().'">';
						echo '<h6>'.get_the_title().'</h6>';
					echo '</a>';
			    echo '</div>';
			echo '</div>';
		}
			else {

			echo '<a href="'. esc_url( get_the_permalink() ) .'" class="date">'.get_the_date().'</a>';
			echo '<a href="'.get_the_permalink().'" class="post">';
			echo '<h6>'.get_the_title().'</h6>';
		    echo '</a>';
		}		

	endwhile;
endif;

echo '</div>';

if ( !empty( $params['readmore_text'] ) ) {

	echo '<a href="'.esc_url($params['readmore_link']).'" class="btn btn-xs">'.esc_html($params['readmore_text']).'</a>';

}	

echo wp_kses_post( $after_widget ) ?>

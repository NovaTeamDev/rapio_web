<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Generating inline css styles for customization
 */

if ( !function_exists('chaitan_generate_css') ) {

	function chaitan_generate_css() {

		// List of attributes
		$css = array(
			'main_color' 			=> true,
			'second_color' 			=> true,			
			'second_lighter_color' 			=> true,			
			'gray_color' 			=> true,
			'white_color' 			=> true,
			'black_color' 			=> true,			
			'footer_color' 			=> true,			

			'nav_bg' 				=> true,
			'nav_opacity_top' 		=> true,
			'nav_opacity_scroll'	=> true,

			'border_radius' 		=> true,
		);

		// Escaping all the attributes
		$css_rgb = array();
		foreach ($css as $key => $item) {

			$css[$key] = esc_attr(fw_get_db_customizer_option($key));
			$css_rgb[$key] = sscanf(esc_attr(fw_get_db_customizer_option($key)), "#%02x%02x%02x");
		}

		// Setting different color scheme for page
		if ( function_exists( 'FW' ) ) {

			$chaitan_color_schemes = array();
			$chaitan_color_schemes_ = fw_get_db_settings_option( 'items' );
			if ( !empty($chaitan_color_schemes) ) {
				foreach ($chaitan_color_schemes_ as $v) {

					$chaitan_color_schemes[$v['slug']] = $v;
				}			
			}
		}

		$chaitan_current_scheme =  apply_filters ('chaitan_current_scheme', array());	
		if ($chaitan_current_scheme == 'default' OR empty($chaitan_current_scheme)) $chaitan_current_scheme = 1;

		if ( function_exists( 'FW' ) AND !empty($chaitan_current_scheme) ) {

			foreach (array(
					'main_color' => 'main-color',
					'second_color' => 'second-color',
					'gray_color' => 'gray-color',
					'black_color' => 'black-color') as $k => $v) {

				if ( !empty($chaitan_color_schemes[$chaitan_current_scheme][$v]) ) {

					$css[$k] = esc_attr($chaitan_color_schemes[$chaitan_current_scheme][$v]);
					$css_rgb[$k] = sscanf(esc_attr($chaitan_color_schemes[$chaitan_current_scheme][$v]), "#%02x%02x%02x");
				}
			}
		}

		include get_template_directory() . '/inc/theme-style/google-fonts.php';		

		$theme_style = "";

		$theme_style .= "
			:root {
			  --black:  {$css['black_color']};
			  --gray:   {$css['gray_color']};
			  --white:  {$css['white_color']};
			  --main:   {$css['main_color']};

			  --font-main:   '{$css['font_main']}';
			  --font-headers: '{$css['font_headers']}';			  
			}		
		";


		$theme_style = str_replace( array( "\n", "\r" ), '', $theme_style );

		return $theme_style;
	}
}

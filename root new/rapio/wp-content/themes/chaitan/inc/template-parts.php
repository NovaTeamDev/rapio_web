<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Functions that displays html parts of theme on output
 */


/**
 * Single comment function
 */
if ( !function_exists( 'chaitan_single_comment' ) ) {

	function chaitan_single_comment( $comment, $args, $depth ) {

		$GLOBALS['comment'] = $comment;
		switch ( $comment->comment_type ) {
			case 'pingback' :
				?>
				<li class="trackback"><?php esc_html_e( 'Trackback:', 'chaitan' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( esc_html__( 'Edit', 'chaitan' ), '<span class="edit-link">', '<span>' ); ?>
				<?php
				break;
			case 'trackback' :
				?>
				<li class="pingback"><?php esc_html_e( 'Pingback:', 'chaitan' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( esc_html__( 'Edit', 'chaitan' ), '<span class="edit-link">', '<span>' ); ?>
				<?php
				break;
			default :
				$author_id = $comment->user_id;
				$author_link = get_author_posts_url( $author_id );
				?>
				<li id="comment-<?php comment_ID(); ?>" <?php comment_class( 'comment_item' ); ?>>
					<div class="comment-single">
						<div class="comment-author-avatar">
							<?php echo get_avatar( $comment, 64 ); ?>
							
						</div>
						<div class="comment-content">
							<div class="comment-info">
	                            <h6 class="comment-author"><?php echo ( ! empty( $author_id ) ? '<a href="' . esc_url( $author_link ) . '">' : '') . comment_author() . ( ! empty( $author_id ) ? '</a>' : ''); ?></h6>

	                            <span class="comment-date-time">
	                            	<span class="comment-date">
	                            		<span class="comment_date_value"><?php echo get_comment_date( get_option( 'date_format' ) ); ?></span>
		                            	<?php echo esc_html__( 'at', 'chaitan' ); ?>
		                            	<span class="comment-time"><?php echo get_comment_date( get_option( 'time_format' ) ); ?></span>
	                            	</span>
	                            </span>
								<?php if ( $comment->comment_approved == 0 ) { ?>
								<div class="comment_not_approved"><?php esc_html_e( 'Your comment is awaiting moderation.', 'chaitan' ); ?></div>
								<?php } ?>	                            
							</div>
							<div class="comment_text_wrap">
								<div class="comment-text"><?php comment_text(); ?></div>
							</div>
							<?php if ( $depth < $args['max_depth'] ) { ?>
								<div class="comment-reply"><?php comment_reply_link( array_merge( $args, array(
									'depth' => $depth,
									'max_depth' => $args['max_depth'],
								) ) ); ?></div>
							<?php } ?>
						</div>
					</div>
				<?php
				break;
		}
	}
}


/**
 * Print html code with footer subscribe section
 */
if ( !function_exists( 'chaitan_the_subscribe_block' ) ) {
	
	function chaitan_the_subscribe_block() {

		global $wp_query;

	    if ( function_exists( 'FW' ) ) {

	    	$subscribe_layout = 'visible';

	        $subscribe_layout_global = fw_get_db_settings_option( 'subscribe' );

	        if ( $subscribe_layout_global == 'visible' ) {

	        	$subscribe_layout = 'visible';
	        }
	            else
	        if ( $subscribe_layout_global == 'hidden' ) {

	        	$subscribe_layout = 'disabled';
	        }

	        if ( $subscribe_layout != 'disabled' ) {

	        	// If default visibility, cheking page settings
	        	$subscribe_layout_page = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'subscribe-layout' );

		        if ( $subscribe_layout_global == 'default' OR $subscribe_layout_page == 'disabled' ) {

		        	$subscribe_layout = $subscribe_layout_page;
		        }
	        	$subscribe_section = get_page_by_path( 'subscribe', OBJECT, 'sections' );
	        }

	        if ( !empty($subscribe_section) AND !empty($subscribe_layout) AND $subscribe_layout != 'disabled' ) {

	            echo '<div class="subscribe-block"><div class="container">'.do_shortcode(wp_kses_post($subscribe_section->post_content)).'</div></div>';
	        }
	    }

	    return true;
	}
}


/**
 * Print html code with topbar section
 */
if ( !function_exists( 'chaitan_the_topbar_block' ) ) {

	function chaitan_the_topbar_block() {

		global $wp_query;

	    if ( function_exists( 'FW' ) ) {

	    	$topbar_layout = 'hidden';
	        $topbar_layout = fw_get_db_settings_option( 'topbar' );

	        if ( $topbar_layout != 'hidden' ) {

	        	$topbar_section = get_page_by_path( 'top-bar', OBJECT, 'sections' );

	        	// If default visibility, cheking page settings
	        	$topbar_layout_page = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'topbar-layout' );

		        if ( $topbar_layout_page == 'disabled' ) {

		        	unset($topbar_section);
		        }
	        }

	        if ( !empty($topbar_section) ) {

	        	if ($topbar_layout == 'desktop') {

	        		$topbar_class = ' hidden-ms hidden-xs hidden-sm';
	        	}
	        		else {

	        		$topbar_class = '';
	        	}

	            echo '<div class="ltx-topbar-block'.esc_attr($topbar_class).'"><div class="container">'.do_shortcode(wp_kses_post($topbar_section->post_content)).'</div></div>';
	        }
	    }

	    return true;
	}
}

/**
 * Prints Footer widgets block
 */
if ( !function_exists( 'chaitan_the_footer_logo_section' ) ) {

	function chaitan_the_footer_logo_section() {

		if ( !function_exists( 'FW' ) ) {

			return false;
		}

		$section_layout = fw_get_db_settings_option( 'footer_top_layout' );
		if ( empty ($section_layout) OR $section_layout == 'hidden' ) {

			return false;
		}

        $chaitan_logo = fw_get_db_settings_option( 'logo' );
		?>
		<section id="ltx-logo-footer">

			<div class="container">
	            <?php

					if ( !empty($chaitan_logo) ) {

						echo '<span class="logo-footer">'.wp_get_attachment_image( $chaitan_logo['attachment_id'], 'full' ).'</span>';
					}

					chaitan_the_social_footer();
	            ?>
			</div>
		</section>
		<?php		
	}
}


/**
 * Prints Footer widgets block
 */
if ( !function_exists( 'chaitan_the_footer_widgets' ) ) {

	function chaitan_the_footer_widgets( $layout = null ) {

		if ( !function_exists( 'FW' ) ) {

			return false;
		}

	    $chaitan_footer_cols = chaitan_get_footer_cols_num();
	    if ( $chaitan_footer_cols['num'] > 0 ): ?>
		<section id="ltx-widgets-footer">
			<div class="container">
				<div class="row">
	                <?php
	                for ($x = 1; $x <= 4; $x++): ?>
	                    <?php if ( !isset($chaitan_footer_cols['hidden'][ $x ]) && is_active_sidebar( 'footer-' . $x ) ): ?>
						<div class="<?php echo esc_attr( $chaitan_footer_cols['classes'][$x] ).' '.esc_attr( $chaitan_footer_cols['hidden_mobile'][$x] ); ?> matchHeight clearfix">    
							<div class="footer-widget-area">
								<?php
	                                dynamic_sidebar( 'footer-' . $x );
	                            ?>
							</div>
						</div>
						<?php endif; ?>
	                <?php
	                endfor; ?>
				</div>
			</div>
		</section>
	    <?php endif;
	}
}


/**
 * Display logo
 */
if ( !function_exists( 'chaitan_the_logo' ) ) {

	function chaitan_the_logo( $layout = null ) {

		echo '<a class="logo" href="'. esc_url( home_url( '/' ) ) .'">';

		if ( function_exists( 'FW' ) ) {

			if ( empty($layout) ) {

				$chaitan_logo = fw_get_db_settings_option( 'logo' );	
			}
				else
			if ( $layout == 'dark' ) {

				$chaitan_logo = fw_get_db_settings_option( 'logo-dark' );	
			}
				else {

				$chaitan_logo = fw_get_db_settings_option( 'logo-center' );	
			}

			$chaitan_current_scheme =  apply_filters ('chaitan_current_scheme', array());
			if ($chaitan_current_scheme == 'default') {

				$chaitan_current_scheme = 1;
			}

			$chaitan_color_schemes = array();
			$chaitan_color_schemes_ = fw_get_db_settings_option( 'items' );
			if ( !empty($chaitan_color_schemes) ) {

				foreach ($chaitan_color_schemes_ as $v) {

					$chaitan_color_schemes[$v['slug']] = $v;
				}			
			}

			if ( !empty($chaitan_current_scheme) AND $chaitan_current_scheme != 'default' ) {

				if (!empty( $chaitan_color_schemes[$chaitan_current_scheme]['logo'])) {

					$chaitan_logo = $chaitan_color_schemes[$chaitan_current_scheme]['logo'];
				}
			}

			if ( !empty( $chaitan_logo ) ) {

				echo wp_get_attachment_image( $chaitan_logo['attachment_id'], 'full' );
			}									
		}

		if ( empty( $chaitan_logo ) ) {

			echo '<img src="'. esc_url( get_template_directory_uri() . '/assets/images/logo.png' ) . '" alt="' . esc_attr( get_bloginfo( 'title' ) ) . '">';
		}

		echo '</a>';
	}
}


/**
 * Prints pageloader overlay, if enabled
 */
if ( !function_exists( 'chaitan_the_pageloader_overlay' ) ) {

	function chaitan_the_pageloader_overlay() {

		if ( function_exists( 'FW' ) ) {

			$pace = fw_get_db_settings_option( 'page-loader' );

			if ( !empty($pace) AND ((!empty($pace['loader']) AND $pace['loader'] != 'disabled') OR 
			   ( !empty($pace) AND !empty($pace['loader']) AND $pace['loader'] != 'disabled') ) ) {

				echo '<div id="preloader"></div>';
			}
		}
	}
}

/**
 * Print copyrights in footer
 */
if ( !function_exists( 'chaitan_the_copyrights' ) ) {

	function chaitan_the_copyrights() {

		if ( function_exists( 'FW' ) ) {

			$chaitan_copyrights = fw_get_db_settings_option( 'copyrights' );

			if ( !empty($chaitan_copyrights) ) {

				echo wp_kses_post( $chaitan_copyrights );	
			}
				else {
				
				echo '<p>'. esc_html__( 'Like-themes &copy; All Rights Reserved - 2018', 'chaitan' ) .'</p>';
			}
		}
			else {

			echo '<p>'. esc_html__( 'Like-themes &copy; All Rights Reserved - 2018', 'chaitan' ) .'</p>';
		}
	}
	
}

/**
 * Footer copyright block
 */
if ( !function_exists( 'chaitan_the_copyrights_section' ) ) {

	function chaitan_the_copyrights_section() {

		?>
		<footer class="copyright-block">
			<div class="container">
	            <?php
	                chaitan_the_copyrights();
	                chaitan_the_go_top();
	            ?>
			</div>
		</footer>
		<?php
	}
}

/**
 * Displays go top icon
 */
if ( !function_exists( 'chaitan_the_go_top' ) ) {

	function chaitan_the_go_top() {

		if ( function_exists( 'FW' ) ) {

	        $chaitan_go_top_hide = fw_get_db_settings_option( 'go_top_hide');
	        $chaitan_go_top_img = fw_get_db_settings_option( 'go_top_img');

			if ( isset($chaitan_go_top_hide) AND $chaitan_go_top_hide !== true ) {

	            if ( empty($chaitan_go_top_img) ) {
	                
	                echo '<a href="#" class="go-top hidden-xs hidden-ms"><span class="fa fa-arrow-up"></span></a>';
	            }
	            	else {

	            	echo '<a href="#" class="go-top go-top-img hidden-xs hidden-ms">';
	                	echo wp_get_attachment_image( $chaitan_go_top_img['attachment_id'], 'full' );
	            	echo '</a>';
	           	}
			}
		}
	}
}

/**
 * Blog related posts
 */
if ( !function_exists( 'chaitan_related_posts' ) ) {

	function chaitan_related_posts($id) {

		if ( !function_exists('FW') ) {

			return false;
		}

		$tags = wp_get_post_tags($id);

		if ( !empty( $tags ) ) {

			set_query_var( 'chaitan_featured_disabled', true );
			echo '<div class="ltx-related blog blog-block layout-two-cols">';
			echo '<h5 class="header-line-after">'.esc_html__( 'Related posts', 'chaitan' ).'</h5>';
			$first_tag = $tags[0]->term_id;

			$args = array(

				'tag__in' => array($first_tag),
				'post__not_in' => array($id),
				'posts_per_page' => 2,
				'ignore_sticky_posts' => 1
			);

			echo '<div class="row">';
			
			$my_query = new WP_Query($args);
			if ( $my_query->have_posts() ) {

				while ($my_query->have_posts()) {

					$my_query->the_post();

					echo '<div class="col-lg-6">';
						get_template_part( 'tmpl/post-formats/list' );				
					echo '</div>';
				}
			}

			echo '</div>';

			echo '</div>';
			wp_reset_query();
			set_query_var( 'chaitan_featured_disabled', false );
		}
	}
}

/**
 * Blog post author info block
 */
if ( !function_exists( 'chaitan_author_bio' ) ) {

	function chaitan_author_bio( ) {
	 
		global $post;
	 
		$content = '';

		if ( is_single() && isset( $post->post_author ) ) {
	 
			$display_name = get_the_author_meta( 'display_name', $post->post_author );

	 		if ( empty( $display_name ) ) {

	 			$display_name = get_the_author_meta( 'nickname', $post->post_author );
	 		}
	 
			$user_description = get_the_author_meta( 'user_description', $post->post_author );

			// No author info, nothing no show
			if ( empty( $user_description ) ) {

				return false;
			}
	 
			$user_website = get_the_author_meta('url', $post->post_author);
	 
			$user_posts = get_author_posts_url( get_the_author_meta( 'ID' , $post->post_author));
	  
			if ( ! empty( $display_name ) ) {

				$author_details = '<p class="author-name">'. esc_html__( 'Author', 'chaitan' ) . '</p><h5>'. $display_name . '</h5>';
			}
	 
			if ( ! empty( $user_description ) ) {

				$author_details .= '<p class="author-details">' . wp_kses_post( nl2br( $user_description ) ). '</p>';
			}
	  
			$author_details .= '<p class="author-links">';

				if ( ! empty( $user_website ) ) {
				 
					$author_details .= '<a href="' . esc_url( $user_website ) .'" target="_blank" rel="nofollow">'. esc_html__( 'Website', 'chaitan' ) . '</a><span class="i">|</span>';
				}

				$author_details .= '<a href="'. esc_url( $user_posts ) .'" class="btn btn-black btn-xs">'. esc_html__( 'All posts', 'chaitan' ) . '</a>';  
		 
			$author_details .= '</p>';

	 
			$content = '<section class="ltx-author-bio">';
				$content .= '<div class="author-image">';
					$content .= get_avatar( get_the_author_meta('user_email'), 90 );
				$content .= '</div>';
				$content .= '<div class="author-info">';
					$content .= $author_details;
				$content .= '</div>';
			$content .= '</section>';
		}

		echo wp_kses_post( $content );
	}
}

/**
 * Displays cats for posts archive
 */

if ( !function_exists( 'chaitan_get_the_cats_archive' ) ) {

	function chaitan_get_the_cats_archive(  ) {

		if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && chaitan_categorized_blog() && sizeof(get_the_category()) <= 3) {

			echo '<span class="ltx-cats">';
			echo get_the_category_list( esc_html_x( ' / ', 'Used between list items, there is a space after the comma.', 'chaitan' ) );
			echo '</span>';
		}
	}
}

/**
 * Displays Blog post info icons block
 * 
 */
if ( !function_exists( 'chaitan_the_post_info' ) ) {

	function chaitan_the_post_info( $authorby = false, $commentsText = false ) {

		$wrapper = true;

	    if ( $wrapper ) {

	    	echo '<ul>';
	    }

	    if ( $authorby ) {

			echo '<li><span class="fa fa-user"></span><span class="info">'.get_the_author_link().'</span></li>';
		}
	    	else {

			echo '<li><a href="'. esc_url( get_the_permalink() ) .'" class="date">'.get_the_date().'</a></li>';

	   	}

		if ( function_exists( 'pvc_post_views' ) ) {

			echo '<li class="icon-fav">
				<span class="fa fa-eye"></span> <i>'.esc_html( strip_tags( pvc_post_views(get_the_ID(), false) ) ) .'</i>
			</li>';
		}
	    
	    if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {

	    	echo '<li class="icon-comments"><span class="fa fa-commenting"></span><a href="'. esc_url( get_the_permalink() ) .'">';

	    	if ( !empty($commentsText) )  {

		    	echo get_comments_number_text( 
		    		esc_html__('0 comments', 'chaitan'), esc_html__('1 comment', 'chaitan'), esc_html__('% comments', 'chaitan')
		    	);
		    }
		    	else {

	    		echo get_comments_number( '0', '1', '%' );
	    	}

	    	echo '</a></li>';
	    }

	    if ( $wrapper ) {

	    	echo '</ul>';
	    }
	}
}

/**
 * Displays Navigation bar icons
 * 
 */
if ( !function_exists( 'chaitan_the_navbar_icons' ) ) {

	function chaitan_the_navbar_icons( $layout = null, $mobile = false ) {

		global $user_ID;

		if ( !function_exists( 'FW' ) ) { return false; }

		$basket_icon = fw_get_db_settings_option( 'basket-icon' );
		$icons = fw_get_db_settings_option( 'navbar-icons' );
		$basket_only = false;

		if ( $layout == 'desktop-center-absolute' OR $mobile ) {

		}
			else
		if ( $layout == 'basket-only' AND $basket_icon == 'mobile' ) {

			$basket_only = true;
		}
			else
		if ( !empty($layout) ) {

			return false;
		}

		$items = '';
		if ( !empty($icons) ) {

			foreach ($icons as $item) {

				if ( !empty( $basket_only ) AND $item['type']['type_radio'] != 'basket' ) continue;

				$li_class = '';
				if ( empty($mobile) AND empty( $basket_only ) ) {

					if ( empty($item['icon-visible']) || $item['icon-visible'] != 'visible-mob' ) {

						$li_class = ' hidden-sm hidden-ms hidden-xs';
					}
						else {

						$li_class = ' hidden-xs';
					}				
				}

				$custom_icon = '';
				if ( $item['icon-type']['icon_radio'] == 'fa' AND !empty($item['icon-type']['fa']['icon_fa']) ) {

					$custom_icon = $item['icon-type']['fa']['icon_fa'];
				}

				if ( $item['type']['type_radio'] == 'search') {

					if ( empty( $custom_icon ) ) { $custom_icon = 'fa-search'; }

					if ( !empty($mobile) ) {

						$id = ' id="top-search-ico-mobile" ';
						$close = '';
					}	
						else {

						$id = ' id="top-search-ico" ';
						$close = '<a href="#" id="top-search-ico-close" class="top-search-ico-close fa fa-close" aria-hidden="true"></a>';
					}

					$items .= '
					<li class="ltx-fa-icon ltx-nav-search  '.esc_attr($li_class).'">
						<div class="top-search">
							<a href="#" '.$id.' class="top-search-ico fa '. esc_attr($custom_icon) .'" aria-hidden="true"></a>
							'.$close.'
							<input placeholder="'.esc_attr__( 'Search', 'chaitan' ).'" value="'. esc_attr( get_search_query() ) .'" type="text">
						</div>
					</li>';
				}

				if ( $item['type']['type_radio'] == 'basket' AND chaitan_is_wc('wc_active')) {

					if ( empty( $custom_icon ) ) { $custom_icon = 'fa-shopping-bag'; }

					$items .= '
						<li class="ltx-fa-icon ltx-nav-cart '.esc_attr($li_class).'">
							<div class="cart-navbar">
								<a href="'. wc_get_cart_url() .'" class="shop_table cart ltx-cart" title="'. esc_attr__( 'View your shopping cart', 'chaitan' ). '">';

									if ( $item['type']['basket']['count'] == 'show' ) {

										$items .= '<span class="cart-contents header-cart-count count">'.WC()->cart->get_cart_contents_count().'</span>';
									}

									$items .= '<i class="fa '. esc_attr($custom_icon) .'" aria-hidden="true"></i>
								</a>
							</div>
						</li>';
				}

				if ( $item['type']['type_radio'] == 'profile' ) {

					if ( empty( $custom_icon ) ) { $custom_icon = 'fa-user'; }

					$header = '';
					$userInfo = get_userdata($user_ID);
					if ( !empty($userInfo) ) $header = $userInfo->user_login;
						else
					if ( empty($userInfo) AND !empty($item['type']['profile']['header']) ) $header = $item['type']['profile']['header'];

					$items .= '
						<li class="ltx-fa-icon ltx-nav-profile menu-item-has-children '.esc_attr($li_class).'">
							<a href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'"><span class="fa '. esc_attr($custom_icon) .'"></span>
							 '.esc_html( $header ).'</a>';

						$items .= '</li>';
				}

				if ( $item['type']['type_radio'] == 'social' AND !empty($custom_icon)) {

					$items .= '
						<li class="ltx-fa-icon ltx-nav-social '.esc_attr($li_class).'">
							<a href="'. esc_url( $item['type']['social']['href'] ) .'" class="fa '. esc_attr($custom_icon) .'" target="_blank">
							</a>
						</li>';
				}	
			}
		}

		if ( !empty($items) ) {

			if ( empty( $mobile ) ) {

				echo '<div class="ltx-navbar-icons"><ul>'.$items.'</ul></div>';
			}
				else {

				echo '<div><ul>'.$items.'</ul></div>';
			}
		}
	}
}


/**
 * Get page breadcrumbs
 */
if ( !function_exists( 'chaitan_the_breadcrumbs' ) ) {

	function chaitan_the_breadcrumbs() {

		if ( function_exists( 'bcn_display' ) && !is_front_page() ) {

			echo '<ul class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">';
			bcn_display_list();
			echo '</ul>';
		}
	}
}

/**
 * Social icons in header
 */
if ( !function_exists( 'chaitan_the_social_header' ) ) {

	function chaitan_the_social_header() {

		if ( function_exists( 'FW' ) ) {
			
			$chaitan_social = fw_get_db_settings_option( 'header-social' );
			$chaitan_social_text = fw_get_db_settings_option( 'header-social-text' );

			if ( $chaitan_social == 'enabled' ) {

				do_shortcode('[chaitan-social text="'.esc_attr( $chaitan_social_text ).'"]');
			}
		}
	}
}
	
/**
 * Social icons in footer
 */
if ( !function_exists( 'chaitan_the_social_footer' ) ) {

	function chaitan_the_social_footer() {

		// In this theme we using same markup as header
		chaitan_the_social_header();
	}
}		

/**
 * Social icons in navbar
 */
if ( !function_exists( 'chaitan_the_navbar_social' ) ) {

	function chaitan_the_navbar_social() {

		global $wp_query;

		if ( function_exists( 'FW' ) ) {

			$navbar_layout = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'navbar-layout' );

			if ( empty( $navbar_layout ) OR $navbar_layout == 'transparent' OR $navbar_layout == 'white' OR $navbar_layout == 'default' ) $navbar_layout = 'desktop';

			if ( !empty( $navbar_layout ) AND $navbar_layout != 'desktop' ) {

				echo '<div class="navbar-social">';
				
				$chaitan_social = fw_get_db_settings_option( 'header-social' );
				$chaitan_social_text = fw_get_db_settings_option( 'header-social-text' );

				if ( $chaitan_social == 'enabled' ) {

					do_shortcode('[chaitan-social text-before="'.esc_attr( $chaitan_social_text ).'"]');
				}

				echo '</div>';
			}
		}
	}
}


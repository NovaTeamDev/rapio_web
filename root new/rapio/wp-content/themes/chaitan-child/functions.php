<?php
/**
 * Theme functions file
 */

/**
 * Enqueue parent theme styles first
 * Replaces previous method using @import
 * <http://codex.wordpress.org/Child_Themes>
 */

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style', 99 );

function enqueue_parent_theme_style() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );

	if ( function_exists( 'fw' ) ) {
		wp_add_inline_style( 'parent-style', chaitan_generate_css() );
	}
}

/**
 * Add your custom functions below
 */

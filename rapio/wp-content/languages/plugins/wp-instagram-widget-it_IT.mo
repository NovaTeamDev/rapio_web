��          �   %   �      @  <   A     ~  %   �  
   �  	   �     �     �  $      $   %     J  	   P     Z     n          �  
   �     �     �  	   �     �  %   �     �  0   �     .  *  ?  E   j     �  &   �     �  	   �       /     0   J  7   {     �     �     �     �     �     �  	             *  	   2     <  0   C     t  0   �     �                     	                                                                            
                             A WordPress widget for showing your latest Instagram photos. Current window (_self) Displays your latest Instagram photos Follow Me! Instagram Instagram Image Instagram did not return a 200. Instagram did not return any images. Instagram has returned invalid data. Large Link text New window (_blank) Number of photos Open links in Original Photo size Scott Evans Small Thumbnail Title Unable to communicate with Instagram. WP Instagram Widget https://github.com/scottsweb/wp-instagram-widget https://scott.ee PO-Revision-Date: 2017-09-24 15:37:39+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: it
Project-Id-Version: Plugins - WP Instagram Widget - Stable (latest release)
 Una widget di WordPress per mostrare le tue ultime foto da Instagram. Finestra corrente (_self) Mostra le tue ultime foto da Instagram Seguimi! Instagram Immagine di Instagram La risposta da Instragram non aveva codice 200. La risposta da Instagram non conteneva immagini. La risposta da Instagram ha restituito dati non validi. Grande Testo del link Nuova finestra (_blank) Numero di foto Apri i link in Dimensioni originali Grandezza Scott Evans Piccola Miniatura Titolo Non è stato possibile comunicare con Instagram. WP Instagram Widget https://github.com/scottsweb/wp-instagram-widget https://scott.ee 
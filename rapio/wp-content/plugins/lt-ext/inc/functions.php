<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/**
 * ltxThemes Plugins Functions
 */

// Get Local Path of include file
function ltxGetLocalPath($file) {

	global $ltx_cfg;

	return $ltx_cfg['path'].$ltx_cfg['base'].$file;
}

// Get Plugin Url
function ltxGetPluginUrl($file) {

	global $ltx_cfg;

	return $ltx_cfg['url'].$file;
}

// Get Visual Composer plugin status
if ( !function_exists( 'ltx_vc_inited' ) ) {

	function ltx_vc_inited() {
		
		return class_exists('Vc_Manager');
	}
}

// Generate img url
if (!function_exists('ltx_get_attachment_img_url')) {
	function ltx_get_attachment_img_url( $img, $size = 'full' ) {
		if ($img > 0) {

			return wp_get_attachment_image_src($img, 'full');
		}
	}
}

if (!function_exists('ltxGetProductsCats')) {
	function ltxGetProductsCats() {

		$taxonomy     = 'product_cat';
		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0;
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$cats = array();
		$all_categories = get_categories( $args );
		foreach ($all_categories as $cat) {

			if (esc_html($cat->name) == 'Uncategorized' OR empty($cat->name) ) continue;

			if ($cat->category_parent == 0) {

			    $cats[$cat->term_id]['href'] = get_term_link($cat->slug, 'product_cat');
			    $cats[$cat->term_id]['name'] = $cat->name;
			}
				else {

			    $cats[$cat->category_parent]['child'][$cat->term_id] = array(

			    	'href' => get_term_link($cat->slug, 'product_cat'),
			    	'name' => $cat->name,
			    );		    
			}
		}	

		return $cats;
	}
}


if (!function_exists('ltxGetCPTCats')) {
	function ltxGetCPTCats($taxonomy) {

		if ( empty($taxonomy) ) return false;

		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0;
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$cats = array();
		$all_categories = get_terms( $args );
		if (!empty($all_categories)) {

			foreach ($all_categories as $cat) {
				if (!empty($cat->category_parent) AND $cat->category_parent == 0) {
				    $category_id = $cat->term_id;       
				    $cats[$cat->term_id] = array(

				    	'href' => get_term_link($cat->slug, $taxonomy),
				    	'name' => $cat->name,
				    );
				}       
			}
		}

		return $cats;
	}
}

if (!function_exists('ltxGetTeamCats')) {
	function ltxGetTeamCats() {

		$taxonomy     = 'team-category';
		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0;
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$cats = array();
		$all_categories = get_terms( $args );
		foreach ($all_categories as $cat) {

			if ($cat->category_parent == 0) {

			    $cats[$cat->term_id]['href'] = get_term_link($cat->slug, 'team-category');
			    $cats[$cat->term_id]['name'] = $cat->name;
			}
				else {

			    $cats[$cat->category_parent]['child'][$cat->term_id] = array(

			    	'href' => get_term_link($cat->slug, 'team-category'),
			    	'name' => $cat->name,
			    );		    
			}
		}	

		return $cats;
	}
}


if (!function_exists('ltxGetSlidersCats')) {
	function ltxGetSlidersCats() {

		$taxonomy     = 'sliders-category';
		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0;
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$cats = array();
		$all_categories = get_terms( $args );
		foreach ($all_categories as $cat) {
			if ($cat->category_parent == 0) {
			    $category_id = $cat->term_id;       
			    $cats[$cat->term_id] = array(

			    	'href' => get_term_link($cat->slug, 'sliders-category'),
			    	'name' => $cat->name,
			    );
			}       
		}	

		return $cats;
	}
}

if (!function_exists('ltxGetPortfolioCats')) {
	function ltxGetPortfolioCats() {

		$taxonomy     = 'portfolio-category';
		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0;
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$cats = array();
		$all_categories = get_terms( $args );
		foreach ($all_categories as $cat) {

			if (!empty($cat->parent) AND $cat->parent == 0) {

			    $cats[$cat->term_id]['href'] = get_term_link($cat->slug, 'portfolio-category');
			    $cats[$cat->term_id]['name'] = $cat->name;
			}
				else {

				if ( !empty($cat->term_id) ) {

				    $cats[$cat->parent]['child'][$cat->term_id] = array(

				    	'href' => get_term_link($cat->slug, 'portfolio-category'),
				    	'name' => $cat->name,
				    );		    
				}
			}   
		}	

		return $cats;
	}
}



if (!function_exists('ltxGetTestimonailsCats')) {
	function ltxGetTestimonailsCats() {

		$taxonomy     = 'testimonials-category';
		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0;
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$cats = array();
		$all_categories = get_terms( $args );
		foreach ($all_categories as $cat) {
			if ($cat->category_parent == 0) {
			    $category_id = $cat->term_id;       
			    $cats[$cat->term_id] = array(

			    	'href' => get_term_link($cat->slug, 'testimonials-category'),
			    	'name' => $cat->name,
			    );
			}       
		}	

		return $cats;
	}
}

if (!function_exists('ltxGetServicesCats')) {
	function ltxGetServicesCats() {

		$taxonomy     = 'services-category';
		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0;
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$cats = array();
		$all_categories = get_terms( $args );
		foreach ($all_categories as $cat) {
			if ($cat->category_parent == 0) {
			    $category_id = $cat->term_id;       
			    $cats[$cat->term_id] = array(

			    	'href' => get_term_link($cat->slug, 'services-category'),
			    	'name' => $cat->name,
			    );
			}       
		}	

		return $cats;
	}
}

if ( !function_exists( 'ltx_is_wc' ) ) {
	/**
	 * Return true|false is woocommerce conditions.
	 *
	 * @param string $tag
	 * @param string|array $attr
	 *
	 * @return bool
	 */
	function ltx_is_wc($tag, $attr='') {
		if( !class_exists( 'woocommerce' ) ) return false;
		switch ($tag) {
			case 'wc_active':
		        return true;
			
		    case 'woocommerce':
		        if( function_exists( 'is_woocommerce' ) && is_woocommerce() ) return true;
				break;
		    case 'shop':
		        if( function_exists( 'is_shop' ) && is_shop() ) return true;
				break;
			case 'product_category':
		        if( function_exists( 'is_product_category' ) && is_product_category($attr) ) return true;
				break;
		    case 'product_tag':
		        if( function_exists( 'is_product_tag' ) && is_product_tag($attr) ) return true;
				break;
		    case 'product':
		    	if( function_exists( 'is_product' ) && is_product() ) return true;
				break;
		    case 'cart':
		        if( function_exists( 'is_cart' ) && is_cart() ) return true;
				break;
		    case 'checkout':
		        if( function_exists( 'is_checkout' ) && is_checkout() ) return true;
				break;
		    case 'account_page':
		        if( function_exists( 'is_account_page' ) && is_account_page() ) return true;
				break;
		    case 'wc_endpoint_url':
		        if( function_exists( 'is_wc_endpoint_url' ) && is_wc_endpoint_url($attr) ) return true;
				break;
		    case 'ajax':
		        if( function_exists( 'is_ajax' ) && is_ajax() ) return true;
				break;
		}

		return false;
	}
}

function ltx_vc_get_metric($item) {

	$pattern = '/^(\d*(?:\.\d+)?)\s*(px|\%|in|cm|mm|em|rem|ex|pt|pc|vw|vh|vmin|vmax)?$/';
	// allowed metrics: http://www.w3schools.com/cssref/css_units.asp
	$regexr = preg_match( $pattern, $item, $matches );
	$value = isset( $matches[1] ) ? (float) $matches[1] : (float) $item;
	$unit = isset( $matches[2] ) ? $matches[2] : 'px';

	return $value . $unit;
}


/**
 * Fix for widgets without header
 */
add_filter( 'dynamic_sidebar_params', 'ltx_check_sidebar_params' );
function ltx_check_sidebar_params( $params ) {
	global $wp_registered_widgets;

	// Exclude for widget with default title
	if ( in_array( $params[0]['widget_name'], array( 'Categories', 'Archives', 'Meta', 'Pages', 'Recent Comments', 'Recent Posts' ) ) ) {

		return $params;
	}

	$settings_getter = $wp_registered_widgets[ $params[0]['widget_id'] ]['callback'][0];
	$settings = $settings_getter->get_settings();
	$settings = $settings[ $params[1]['number'] ];

	if ( $params[0]['after_widget'] === '</div></aside>' && isset( $settings['title'] ) && empty( $settings['title'] ) ) {
		$params[0]['before_widget'] .= '<div class="content">';
	}

	return $params;
}


/**
 * Cuts text by the number of characters
 */
if ( !function_exists( 'ltx_cut_text' ) ) {

	function ltx_cut_text( $text, $cut = 300, $aft = ' ...' ) {
		if ( empty( $text ) ) {
			return null;
		}

		if ( empty($cut) AND function_exists( 'fw' ) ) {
			$cut = (int) fw_get_db_settings_option( 'excerpt_wc_auto' );
		}

		$text = wp_strip_all_tags( $text, true );
		$text = strip_tags( $text );
		$text = preg_replace( "/<p>|<\/p>|<br>|(( *&nbsp; *)|(\s{2,}))|\\r|\\n/", ' ', $text );
		if ( mb_strlen( $text ) > $cut ) {
			$text = mb_substr( $text, 0, $cut, 'UTF-8' );
			return mb_substr( $text, 0, mb_strripos( $text, ' ', 0, 'UTF-8' ), 'UTF-8' ) . $aft;
		} else {
			return $text;
		}
	}
}


function ltx_get_team() {

	$query_args = array(
		'post_type' => 'team',
		'post_status' => 'publish',
		'posts_per_page' => 100,
	);

	$query = new WP_Query( $query_args );

	$team = array('' => '---');
	if ( $query->have_posts() ) {

		foreach ( $query->posts as $q ) {

			$teamId = $q->ID;
			$teamTitle = $q->post_title;
			$team[$teamTitle] = $teamId;
			
		}		
	}	

	wp_reset_postdata();

	return $team;
}

function ltx_get_team_full() {

	$query_args = array(
		'post_type' => 'team',
		'post_status' => 'publish',
		'posts_per_page' => 100,
	);

	$query = new WP_Query( $query_args );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();

			$teamId = get_the_ID();
			$teamTitle = get_the_title();
			$subheader = fw_get_db_post_option(get_The_ID(), 'subheader');
			$photo = wp_get_attachment_image( get_post_thumbnail_id( get_The_ID()) , 'chaitan-tiny-square');

			$team[$teamId] = array(
				
				'title' 	=> get_the_title(),
				'subheader' => $subheader,
				'photo' 	=> $photo,
			);
		}		
	}	

	wp_reset_postdata();

	return $team;

}


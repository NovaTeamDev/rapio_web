<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/**
 * Config
 */

$ltx_cfg = array(

	'path'	=> plugin_dir_path(__DIR__),
	'base' 	=> plugin_basename(__DIR__),
	'url'	=> plugin_dir_url(__FILE__),

	'ltx_sections'	=> array(),
);


add_action( 'after_setup_theme', 'ltx_vc_config', 4 );
if ( !function_exists('ltx_vc_config')) {

	function ltx_vc_config() {

		global $ltx_cfg;

	    $value = array();
	    $value = apply_filters( 'ltx_get_vc_config', $value );

	    $ltx_cfg = array_merge($ltx_cfg, $value);

	    return $value;
	}
}

add_action( 'plugins_loaded', 'ltx_load_plugin_textdomain' );
if ( !function_exists('ltx_load_plugin_textdomain')) {
	function ltx_load_plugin_textdomain() {
		load_plugin_textdomain( 'lt-ext', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
	}
}

<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/**
 * Services Shortcode
 */

$args = get_query_var('like_sc_services');

$class = '';
if ( !empty($args['class']) ) $class .= ' '. esc_attr($args['class']);
if ( !empty($args['id']) ) $id = ' id="'. esc_attr($args['id']). '"'; else $id = '';

$class .= ' layout-'.$args['layout'];

$query_args = array(
	'post_type' => 'services',
	'post_status' => 'publish',
	'posts_per_page' => (int)($args['limit']),
);

if ( !empty($args['ids']) ) $query_args['post__in'] = explode(',', esc_attr($args['ids']));
	else
if ( !empty($args['cat']) ) {

	$query_args['tax_query'] = 	array(
		array(
            'taxonomy'  => 'services-category',
            'field'     => 'if', 
            'terms'     => array(esc_attr($args['cat'])),
		)
    );
}

$query = new WP_Query( $query_args );

if ( $query->have_posts() ) {

	echo '<div class="services-sc '.esc_attr($class).' row">';

	if ( $args['layout'] == 'photos') {

		echo '<div class="swiper-container services-slider" data-cols="3" data-autoplay="0">
				<div class="swiper-wrapper">';		

	}

	while ( $query->have_posts() ):

		$query->the_post();

		$header = fw_get_db_post_option(get_The_ID(), 'header');
		$cut = fw_get_db_post_option(get_The_ID(), 'cut');
		$link = fw_get_db_post_option(get_The_ID(), 'link');
		$icon = fw_get_db_post_option(get_The_ID(), 'icon');

		if ( !empty( $header) )  {

			$header = str_replace(array('{{', '}}'), array('<span>', '</span>'), $header);
		}
			else {

			$header = get_the_title();
		}

		if ( empty($link) ) {

			$link = get_the_permalink();
		}

		/**
		 * Slider with photos
		 */
		if ( $args['layout'] == 'photos'):

		$item_class = ' col-lg-4 col-sm-4 ';
	?>
	<div class="swiper-slide item <?php echo esc_attr( $item_class ); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class( ); ?> data-mh="ltx-services">
		    <a href="<?php echo esc_url( $link ); ?>" class="photo">
		        <?php
		        	echo wp_get_attachment_image( get_post_thumbnail_id( get_The_ID()) , 'chaitan-service' );
					echo '<div class="descr">
						<h5 class="header">'.wp_kses_post( $header ).'</h5>
						<p>'.wp_kses_post( $cut ).'</p>
					</div>';

					if ( !empty($args['image']) ) {

						echo '<span class="watermark">'.wp_get_attachment_image( $args['image'] , 'full' ).'</span>';
					}

		        ?>
		    </a>
		</article>
	</div>
	<?php
		/**
		 * 3 icons in a row
		 */
		elseif ( $args['layout'] == 'icon-row' ):

		$item_class = ' col-lg-4 col-sm-4 ';
	?>
	<div class="item <?php echo esc_attr( $item_class ); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class( ); ?> data-mh="ltx-services">
		    <a href="<?php echo esc_url( $link ); ?>" class="photo">
		        <?php
		        	echo wp_get_attachment_image( $icon['attachment_id'] , 'full' );
					echo '<h6 class="header">'.wp_kses_post( $header ).'</h6>';
		        ?>
		    </a>
		</article>
	</div>
	<?php
		endif;
	endwhile;

	if ( $args['layout'] == 'photos') {
		echo '
		</div>
			<div class="arrows">
				<a href="#" class="arrow-left fa fa-arrow-left"></a>
				<a href="#" class="arrow-right fa fa-arrow-right"></a>
			</div>
		</div>';
	}


	wp_reset_postdata();


	echo '</div>';
}


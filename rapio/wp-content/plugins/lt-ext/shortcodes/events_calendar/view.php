<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/**
 * Events Shortcode
 */

$args = get_query_var('like_sc_events_calendar');

$query_args = array(
	'post_type' => 'tribe_events',
	'post_status' => 'publish',
	'posts_per_page' => (int)($atts['limit']),
);

$class = '';
if ( !empty($args['class']) ) $class .= ' '. esc_attr($args['class']);
if ( !empty($args['id']) ) $id = ' id="'. esc_attr($args['id']). '"'; else $id = '';

if ( !empty($args['cat']) ) {

	$query_args['tax_query'] = 	array(
			array(
	            'taxonomy'  => 'tribe_events_cat',
	            'field'     => 'if', 
	            'terms'     => array(esc_attr($args['cat'])),
			)
    );
}

$query = new WP_Query( $query_args );

if ( $query->have_posts() ) {

	$cols = 1;

	echo '<div class="events-sc '.esc_attr($class).'" '.$id.'>';

	while ( $query->have_posts() ) {

		$query->the_post();
	//	$subheader = fw_get_db_post_option(get_The_ID(), 'subheader');

		$date = array();
		if (function_exists('tribe_get_start_date')) {

			$date['d'] = tribe_get_start_date(get_The_ID(), false, 'd');
			$date['F'] = tribe_get_start_date(get_The_ID(), false, 'F, ‘y');
		}

		echo '<div class="item">';
			echo '<div class="row">';

				echo '<div class="col-md-2">';
					echo '<div class="in date matchHeight">';
						echo '<div><span class="date-day">'.esc_html($date['d']).'</span><span class="date-my">'.esc_html($date['F']).'</span></div>';
					echo '</div>';
				echo '</div>';

				echo '<div class="col-md-8">
						<div class="in name matchHeight">
							<div>
								<h6 class="header"><a href="'.get_the_permalink().'">'. get_the_title() .'</a></h6>
							</div>
						</div>
					</div>';
				echo '<div class="col-md-2 div-more"><div class="in matchHeight"><span class="href-arrow"><a href="'.get_the_permalink().'">'.esc_html($atts['btn_text']).'</a></span></div></div>';
			echo '</div>';
		echo '</div>';
	}

	echo '</div>';

	wp_reset_postdata();
}


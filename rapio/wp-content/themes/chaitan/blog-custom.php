<?php
/**
 * The blog template file
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */

$chaitan_layout = '';
$chaitan_sidebar_hidden = false;
$chaitan_sidebar = 'right';
$blog_wrap_class = 'col-lg-8 col-md-8 col-xs-12 matchHeight';


if ( function_exists( 'FW' ) ) {

	$chaitan_layout = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'blog-layout' );
	$chaitan_sidebar = fw_get_db_settings_option( 'blog_list_sidebar' );

	$chaitan_sidebar_custom = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'sidebar-layout' );
	if ( $chaitan_sidebar_custom != 'default') $chaitan_sidebar = $chaitan_sidebar_custom;

	if ( $chaitan_sidebar == 'hidden' ) $chaitan_sidebar_hidden = true;

	if ($chaitan_layout == 'three-cols') {

		$chaitan_sidebar_hidden = true;
	}

	if ( $chaitan_sidebar == 'left' ) $blog_wrap_class = 'col-xl-8 col-xl-push-4 col-lg-8 col-lg-push-4 col-lg-offset-0 col-md-12 col-xs-12 matchHeight';

	$blog_class = '';
	if ( $chaitan_layout == 'two-cols' OR $chaitan_layout == 'three-cols' ) {

		$blog_class = 'masonry';
		if ( $chaitan_sidebar_hidden ) $blog_wrap_class = 'col-lg-12 col-xs-12';
	}
		else {

		if ( $chaitan_sidebar_hidden ) $blog_wrap_class = 'col-xl-8 col-lg-10 col-md-12 col-xs-12';	
	}
}

get_header(); ?>
<div class="inner-page margin-top">
	<div class="row row-eq-height_ <?php if ( $chaitan_sidebar_hidden ) echo 'centered'; ?>">
        <div class="<?php echo esc_attr( $blog_wrap_class ); ?>" data-mh="ltx-col">
            <div class="blog blog-block layout-<?php echo esc_attr($chaitan_layout); ?>">
				<?php

				if ( get_query_var( 'paged' ) ) {

					$paged = get_query_var( 'paged' );

				} elseif ( get_query_var( 'page' ) ) {

					$paged = get_query_var( 'page' );
					
				} else {

					$paged = 1;
				}

				if (isset($_GET['s'])) {

					$wp_query = new WP_Query( array(
						's'		=> esc_sql( $_GET['s'] ),
						'paged' => (int) $paged,
					) );
				}
					else {

					$wp_query = new WP_Query( array(
						'post_type' => 'post',
						'paged' => (int) $paged,
					) );
				}

            	echo '<div class="row '.esc_attr($blog_class).'">';
				if ( $wp_query->have_posts() ) :

					while ( $wp_query->have_posts() ) : the_post();

						if ( !function_exists( 'fw_get_db_settings_option' ) ) {

							get_template_part( 'tmpl/content-post-one-col', $wp_query->get_post_format() );
						}
							else {

							set_query_var( 'chaitan_layout', $chaitan_layout );

							if ($chaitan_layout == 'three-cols') {

								get_template_part( 'tmpl/content-post-three-cols', $wp_query->get_post_format() );
							}
								else
							if ($chaitan_layout == 'two-cols') {

								get_template_part( 'tmpl/content-post-two-cols', $wp_query->get_post_format() );
							}
								else {

								get_template_part( 'tmpl/content-post-one-col', $wp_query->get_post_format() );
							}
						}

						endwhile;

					else :
						// If no content, include the "No posts found" template.
						get_template_part( 'tmpl/content', 'none' );

					endif;
				echo '</div>';
				?>
	        </div>
			<?php
			if ( have_posts() ) {

				chaitan_paging_nav();
			}
            ?>	        
	    </div>
	    <?php
	    if ( !$chaitan_sidebar_hidden ) {

            if ( $chaitan_sidebar == 'left' ) {

            	get_sidebar( 'left' );
            }
            	else  {

            	get_sidebar();
            }
	    }
	    ?>
	</div>
</div>
<?php

get_footer();

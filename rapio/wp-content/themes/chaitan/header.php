<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head>
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta name="format-detection" content="telephone=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php

	chaitan_the_pageloader_overlay();
	get_template_part( 'navbar' ); 

	$pageheader_layout = chaitan_get_pageheader_layout();
	$chaitan_header_class = chaitan_get_pageheader_class();

	if ( $pageheader_layout != 'disabled' ) : ?>
	<header class="page-header  <?php echo esc_attr($chaitan_header_class); ?>">
	    <div class="container">   
	    	<?php
	    		chaitan_the_h1();			
				chaitan_the_breadcrumbs();
			?>	    
	    </div>
    	<?php
			chaitan_the_social_header();
		?>	 	    
	</header>
	<?php endif; ?>
	<div class="container main-wrapper">
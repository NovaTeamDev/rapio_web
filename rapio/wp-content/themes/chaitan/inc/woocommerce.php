<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Woocommerce Hooks
 */

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

remove_action( 'woocommerce_before_shop_loop_item',	'woocommerce_template_loop_product_link_open', 10);
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

remove_action( 'woocommerce_before_subcategory', 'woocommerce_template_loop_category_link_open', 10);
remove_action( 'woocommerce_after_subcategory',	'woocommerce_template_loop_category_link_close', 10);


add_filter( 'woocommerce_show_page_title', '__return_false' );

add_action('woocommerce_before_main_content', 'chaitan_wc_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'chaitan_wc_wrapper_end', 10);

if ( !function_exists( 'chaitan_wc_wrapper_start' ) ) {
	function chaitan_wc_wrapper_start() {

		$chaitan_sidebar = chaitan_get_wc_sidebar_pos();

		if ( is_active_sidebar( 'sidebar-wc' ) AND !empty( $chaitan_sidebar ) ) {

	  		echo '<div class="inner-page margin-top">
	  				<div class="row row-eq-height_">';
	  		
	  		if ( $chaitan_sidebar == 'left' ) {

		  		echo '<div class="col-xl-8 col-xl-push-4 col-lg-8 col-lg-push-4 col-md-12 text-page products-column-with-sidebar matchHeight" data-mh="ltx-col">';
	  		}
	  			else {

	  			echo '<div class="col-xl-8 col-lg-8 col-md-12 col-xs-12 text-page products-column-with-sidebar matchHeight" data-mh="ltx-col">';
  			}
		}
			else {

	  		echo '<div class="inner-page margin-default">
	  				<div class="row centered"><div class="col-xl-9 col-lg-12 text-page">';
		}	  
	}
}

if ( !function_exists( 'chaitan_wc_wrapper_end' ) ) {
	function chaitan_wc_wrapper_end() {
	  echo '</div>';
	}
}


remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

add_action( 'woocommerce_before_subcategory_title',	'chaitan_woocommerce_item_wrapper_start', 9 );
add_action( 'woocommerce_before_shop_loop_item_title', 'chaitan_woocommerce_item_wrapper_start', 9 );

add_action( 'woocommerce_before_subcategory_title',	'chaitan_woocommerce_title_wrapper_start', 20 );
add_action( 'woocommerce_before_shop_loop_item_title', 'chaitan_woocommerce_title_wrapper_start', 20 );

add_action( 'woocommerce_after_shop_loop_item_title',	'chaitan_woocommerce_title_wrapper_end', 7);

add_action( 'woocommerce_after_subcategory', 'chaitan_woocommerce_item_wrapper_end', 20 );
add_action( 'woocommerce_after_shop_loop_item',	'chaitan_woocommerce_item_wrapper_end', 20 );

function chaitan_add_star_rating() {


}

if ( !function_exists( 'chaitan_woocommerce_item_wrapper_start' ) ) {

	function chaitan_woocommerce_item_wrapper_start($cat='') {

		global $product;

		echo '<div data-mh="ltx-woocommerce-item" class="matchHeight item">';
		?>
			<a href="<?php echo esc_url(is_object($cat) ? get_term_link($cat->slug, 'product_cat') : get_permalink()); ?>">
				<div class="image">
		<?php
	}
}

if ( !function_exists( 'chaitan_woocommerce_item_wrapper_end' ) ) {

	function chaitan_woocommerce_item_wrapper_end($cat='') {

		echo '</div>';
	}
}

if ( !function_exists( 'chaitan_woocommerce_title_wrapper_start' ) ) {

	function chaitan_woocommerce_title_wrapper_start($cat='') {

		global $product;

		echo '</div>';

		echo wc_get_rating_html( $product->get_average_rating() );	

	}
}

if ( !function_exists( 'chaitan_woocommerce_title_wrapper_end' ) ) {

	function chaitan_woocommerce_title_wrapper_end() {

		global $product;

		echo '</a>';		

		echo '<a href="'.get_permalink( $product->get_id() ).'" class="btn btn-white btn-more">'.esc_html__( 'More info', 'chaitan' ).'</a>';	


		if ((is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy()) && !is_product()) {

		    $excerpt = apply_filters('the_excerpt', get_the_excerpt());

		    if ( function_exists('FW') ){

				$cut = (int) fw_get_db_settings_option( 'excerpt_wc_auto' );
		    }
			if (empty($cut)) $cut = 50;

			echo '<div class="post_content entry-content">'. wp_kses_post( chaitan_cut_text( $excerpt, $cut ) ) .'</div>';
		}
	}
}

add_filter( 'post_class', 'chaitan_woocommerce_loop_shop_columns_class' );
add_filter( 'product_cat_class', 'chaitan_woocommerce_loop_shop_columns_class', 10, 3 );

if ( !function_exists( 'chaitan_woocommerce_loop_shop_columns_class' ) ) {
	function chaitan_woocommerce_loop_shop_columns_class($classes, $class='', $cat='') {
		global $woocommerce_loop;

		return $classes;
	}
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	
	$chaitanWoocommerceNewLabel = new chaitanWoocommerceNewLabel();
}

/*
	New Label
*/
if ( !class_exists( 'chaitan_woocommerce_new_label' ) ) {

	class chaitanWoocommerceNewLabel {

		public function __construct() {

			$this->settings = array(

				array(
					'id' => 'wc_nb_options',
					'type' => 'title',
					'name' => esc_html__( 'Label New', 'chaitan' ),
				),
				array(
					'id' 		=> 'wc_new_label_days',
					'type' 		=> 'number',
					'name' 		=> esc_html__( 'Show New Products Days', 'chaitan' ),
				),
				array(
					'type' => 'sectionend',
					'id' => 'wc_new_label_options',
				),
			);

			add_option( 'wc_new_label_days', '30' );

			add_action( 'woocommerce_settings_image_options_after', array( $this, 'chaitan_woocommerce_admin_settings' ), 20 );
			add_action( 'woocommerce_update_options_catalog', array( $this, 'chaitan_woocommerce_save_admin_settings' ) );
			add_action( 'woocommerce_update_options_products', array( $this, 'chaitan_woocommerce_save_admin_settings' ) );

			add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'chaitan_woocommerce_product_loop_new_label' ), 30 );
		}

		function chaitan_woocommerce_product_loop_new_label() {

			$product_date = strtotime( get_the_time( 'Y-m-d' ) );
			$new_days = get_option( 'wc_new_label_days' );
			$item = wc_get_product( get_the_ID() );

			if ( empty($new_days)) {

				$new_days = 0;
			}

			if ( !$item->is_on_sale() AND ( time() - ( 60 * 60 * 24 * $new_days ) ) < $product_date ) {

				echo '<span class="wc-label-new">' . esc_html__( 'New', 'chaitan' ) . '</span>';
			}
		}

		function chaitan_woocommerce_admin_settings() {

			woocommerce_admin_fields( $this->settings );
		}

		function chaitan_woocommerce_save_admin_settings() {

			woocommerce_update_options( $this->settings );
		}

	}
}


function chaitan_related_products_limit() {

	global $product;
	
	$args['posts_per_page'] = 3;
	return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'chaitan_related_products_args' );
function chaitan_related_products_args( $args ) {
	$args['posts_per_page'] = 3;
	$args['columns'] = 2;
	return $args;
}


<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * WPBakery Visual Composer configuration
 */

if ( chaitan_is_vc() ) {

	/**
	 * Changing path of templates
	 */
	if(function_exists('vc_set_shortcodes_templates_dir')) {

		$dir = get_template_directory_uri() . '/vc-templates';
		vc_set_shortcodes_templates_dir( $dir );
	}
}


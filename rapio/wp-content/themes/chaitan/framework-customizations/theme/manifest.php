<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['id'] = 'chaitan';

$manifest['supported_extensions'] = array(
	'backups' => array(),
	'seo' => array(),
	'megamenu' => array(),
);

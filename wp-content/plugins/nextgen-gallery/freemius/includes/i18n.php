<?php
	/**
	 * @package     Freemius
	 * @copyright   Copyright (c) 2015, Freemius, Inc.
	 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
	 * @since       1.1.4
	 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	/**
	 * All strings can now be overridden.
	 *
	 * For example, if we want to override:
	 *      'you-are-step-away' => 'You are just one step away - %s',
	 *
	 * We can use the filter:
	 *      fs_override_i18n( array(
	 *          'opt-in-connect' => __( "Yes - I'm in!", '{your-text_domain}' ),
	 *          'skip'           => __( 'Not today', '{your-text_domain}' ),
	 *      ), '{plugin_slug}' );
	 *
	 * Or with the Freemius instance:
	 *
	 *      my_freemius->override_i18n( array(
	 *          'opt-in-connect' => __( "Yes - I'm in!", '{your-text_domain}' ),
	 *          'skip'           => __( 'Not today', '{your-text_domain}' ),
	 *      );
	 */
	global $fs_text;

	$fs_text = array(
		'account'                       => __( 'Account', 'freemius' ),
		'addon'                         => __( 'Add On', 'freemius' ),
		'contact-us'                    => __( 'Contact Us', 'freemius' ),
		'contact-support'               => __( 'Contact Support', 'freemius' ),
		'change-ownership'              => __( 'Change Ownership', 'freemius' ),
		'support'                       => __( 'Support', 'freemius' ),
		'support-forum'                 => __( 'Support Forum', 'freemius' ),
		'add-ons'                       => __( 'Add Ons', 'freemius' ),
		'upgrade'                       => _x( 'Upgrade', 'verb', 'freemius' ),
		'awesome'                       => __( 'Awesome', 'freemius' ),
		'pricing'                       => _x( 'Pricing', 'noun', 'freemius' ),
		'price'                         => _x( 'Price', 'noun', 'freemius' ),
		'unlimited-updates'             => __( 'Unlimited Updates', 'freemius' ),
		'downgrade'                     => _x( 'Downgrade', 'verb', 'freemius' ),
		'cancel-trial'                  => __( 'Cancel Trial', 'freemius' ),
		'free-trial'                    => __( 'Free Trial', 'freemius' ),
		'start-free-x'                  => __( 'Start my free %s', 'freemius' ),
		'no-commitment-x'               => __( 'No commitment for %s - cancel anytime', 'freemius' ),
		'after-x-pay-as-little-y'       => __( 'After your free %s, pay as little as %s', 'freemius' ),
		'details'                       => __( 'Details', 'freemius' ),
		'account-details'               => __( 'Account Details', 'freemius' ),
		'delete'                        => _x( 'Delete', 'verb', 'freemius' ),
		'show'                          => _x( 'Show', 'verb', 'freemius' ),
		'hide'                          => _x( 'Hide', 'verb', 'freemius' ),
		'edit'                          => _x( 'Edit', 'verb', 'freemius' ),
		'update'                        => _x( 'Update', 'verb', 'freemius' ),
		'date'                          => __( 'Date', 'freemius' ),
		'amount'                        => __( 'Amount', 'freemius' ),
		'invoice'                       => __( 'Invoice', 'freemius' ),
		'billing'                       => __( 'Billing', 'freemius' ),
		'payments'                      => __( 'Payments', 'freemius' ),
		'delete-account'                => __( 'Delete Account', 'freemius' ),
		'dismiss'                       => _x( 'Dismiss', 'as close a window', 'freemius' ),
		'plan'                          => _x( 'Plan', 'as product pricing plan', 'freemius' ),
		'change-plan'                   => __( 'Change Plan', 'freemius' ),
		'download-x-version'            => _x( 'Download %s Version', 'as download professional version', 'freemius' ),
		'download-x-version-now'        => _x( 'Download %s version now', 'as download professional version now',
			'freemius' ),
		'download-latest'               => _x( 'Download Latest', 'as download latest version', 'freemius' ),
		'you-have-x-license'            => _x( 'You have a %s license.', 'E.g. you have a professional license.',
			'freemius' ),
		'new'                           => __( 'New', 'freemius' ),
		'free'                          => __( 'Free', 'freemius' ),
		'trial'                         => _x( 'Trial', 'as trial plan', 'freemius' ),
		'start-trial'                   => _x( 'Start Trial', 'as starting a trial plan', 'freemius' ),
		'purchase'                      => _x( 'Purchase', 'verb', 'freemius' ),
		'purchase-license'              => __( 'Purchase License', 'freemius' ),
		'buy'                           => _x( 'Buy', 'verb', 'freemius' ),
		'buy-license'                   => __( 'Buy License', 'freemius' ),
		'license-single-site'           => __( 'Single Site License', 'freemius' ),
		'license-unlimited'             => __( 'Unlimited Licenses', 'freemius' ),
		'license-x-sites'               => __( 'Up to %s Sites', 'freemius' ),
		'renew-license-now'             => __( '%sRenew your license now%s to access version %s features and support.',
			'freemius' ),
		'ask-for-upgrade-email-address' => __( "Enter the email address you've used for the upgrade below and we will resend you the license key.",
			'freemius' ),
		'x-plan'                        => _x( '%s Plan', 'e.g. Professional Plan', 'freemius' ),
		'you-are-step-away'             => __( 'You are just one step away - %s', 'freemius' ),
		'activate-x-now'                => _x( 'Complete "%s" Activation Now',
			'%s - plugin name. As complete "Jetpack" activation now', 'freemius' ),
		'few-plugin-tweaks'             => __( 'We made a few tweaks to the plugin, %s', 'freemius' ),
		'optin-x-now'                   => __( 'Opt-in to make "%s" Better!', 'freemius' ),
		'error'                         => __( 'Error', 'freemius' ),
		'failed-finding-main-path'      => __( 'Freemius SDK couldn\'t find the plugin\'s main file. Please contact sdk@freemius.com with the current error.',
			'freemius' ),
		#region Account

		'expiration'                   => _x( 'Expiration', 'as expiration date', 'freemius' ),
		'license'                      => _x( 'License', 'as software license', 'freemius' ),
		'not-verified'                 => __( 'not verified', 'freemius' ),
		'verify-email'                 => __( 'Verify Email', 'freemius' ),
		'expires-in'                   => _x( 'Expires in %s', 'e.g. expires in 2 months', 'freemius' ),
		'renews-in'                    => _x( 'Auto renews in %s', 'e.g. auto renews in 2 months', 'freemius' ),
		'no-expiration'                => __( 'No expiration', 'freemius' ),
		'expired'                      => __( 'Expired', 'freemius' ),
		'cancelled'                    => __( 'Cancelled', 'freemius' ),
		'in-x'                         => _x( 'In %s', 'e.g. In 2 hours', 'freemius' ),
		'x-ago'                        => _x( '%s ago', 'e.g. 2 min ago', 'freemius' ),
		'version'                      => _x( 'Version', 'as plugin version', 'freemius' ),
		'name'                         => __( 'Name', 'freemius' ),
		'email'                        => __( 'Email', 'freemius' ),
		'email-address'                => __( 'Email address', 'freemius' ),
		'verified'                     => __( 'Verified', 'freemius' ),
		'plugin'                       => __( 'Plugin', 'freemius' ),
		'plugins'                      => __( 'Plugins', 'freemius' ),
		'themes'                       => __( 'Themes', 'freemius' ),
		'path'                         => _x( 'Path', 'as file/folder path', 'freemius' ),
		'title'                        => __( 'Title', 'freemius' ),
		'free-version'                 => __( 'Free version', 'freemius' ),
		'premium-version'              => __( 'Premium version', 'freemius' ),
		'slug'                         => _x( 'Slug', 'as WP plugin slug', 'freemius' ),
		'id'                           => __( 'ID', 'freemius' ),
		'users'                        => __( 'Users', 'freemius' ),
		'plugin-installs'              => __( 'Plugin Installs', 'freemius' ),
		'sites'                        => _x( 'Sites', 'like websites', 'freemius' ),
		'user-id'                      => __( 'User ID', 'freemius' ),
		'site-id'                      => __( 'Site ID', 'freemius' ),
		'public-key'                   => __( 'Public Key', 'freemius' ),
		'secret-key'                   => __( 'Secret Key', 'freemius' ),
		'no-secret'                    => _x( 'No Secret', 'as secret encryption key missing', 'freemius' ),
		'no-id'                        => __( 'No ID', 'freemius' ),
		'sync-license'                 => _x( 'Sync License', 'as synchronize license', 'freemius' ),
		'sync'                         => _x( 'Sync', 'as synchronize', 'freemius' ),
		'activate-license'             => __( 'Activate License', 'freemius' ),
		'activate-free-version'        => __( 'Activate Free Version', 'freemius' ),
		'activate-license-message'     => __( 'Please enter the license key that you received in the email right after the purchase:',
			'freemius' ),
		'activating-license'           => __( 'Activating license...', 'freemius' ),
		'change-license'               => __( 'Change License', 'freemius' ),
		'update-license'               => __( 'Update License', 'freemius' ),
		'deactivate-license'           => __( 'Deactivate License', 'freemius' ),
		'activate'                     => __( 'Activate', 'freemius' ),
		'deactivate'                   => __( 'Deactivate', 'freemius' ),
		'skip-deactivate'              => __( 'Skip & Deactivate', 'freemius' ),
		'no-deactivate'                => __( 'No - just deactivate', 'freemius' ),
		'yes-do-your-thing'            => __( 'Yes - do your thing', 'freemius' ),
		'active'                       => _x( 'Active', 'active mode', 'freemius' ),
		'is-active'                    => _x( 'Is Active', 'is active mode?', 'freemius' ),
		'install-now'                  => __( 'Install Now', 'freemius' ),
		'install-update-now'           => __( 'Install Update Now', 'freemius' ),
		'more-information-about-x'     => __( 'More information about %s', 'freemius' ),
		'localhost'                    => __( 'Localhost', 'freemius' ),
		'activate-x-plan'              => _x( 'Activate %s Plan', 'as activate Professional plan', 'freemius' ),
		'x-left'                       => _x( '%s left', 'as 5 licenses left', 'freemius' ),
		'last-license'                 => __( 'Last license', 'freemius' ),
		'what-is-your-x'               => __( 'What is your %s?', 'freemius' ),
		'activate-this-addon'          => __( 'Activate this add-on', 'freemius' ),
		'deactivate-license-confirm'   => __( 'Deactivating your license will block all premium features, but will enable you to activate the license on another site. Are you sure you want to proceed?',
			'freemius' ),
		'delete-account-x-confirm'     => __( 'Deleting the account will automatically deactivate your %s plan license so you can use it on other sites. If you want to terminate the recurring payments as well, click the "Cancel" button, and first "Downgrade" your account. Are you sure you would like to continue with the deletion?',
			'freemius' ),
		'delete-account-confirm'       => __( 'Deletion is not temporary. Only delete if you no longer want to use this plugin anymore. Are you sure you would like to continue with the deletion?',
			'freemius' ),
		'downgrade-x-confirm'          => __( 'Downgrading your plan will immediately stop all future recurring payments and your %s plan license will expire in %s.',
			'freemius' ),
		'cancel-trial-confirm'         => __( 'Cancelling the trial will immediately block access to all premium features. Are you sure?',
			'freemius' ),
		'after-downgrade-non-blocking' => __( 'You can still enjoy all %s features but you will not have access to plugin updates and support.',
			'freemius' ),
		'after-downgrade-blocking'     => __( 'Once your license expire you can still use the Free version but you will NOT have access to the %s features.',
			'freemius' ),
		'proceed-confirmation'         => __( 'Are you sure you want to proceed?', 'freemius' ),
		#endregion Account

		'add-ons-for-x'                            => __( 'Add Ons for %s', 'freemius' ),
		'add-ons-missing'                          => __( 'We could\'nt load the add-ons list. It\'s probably an issue on our side, please try to come back in few minutes.',
			'freemius' ),
		#region Plugin Deactivation
		'anonymous-feedback'                       => __( 'Anonymous feedback', 'freemius' ),
		'quick-feedback'                           => __( 'Quick feedback', 'freemius' ),
		'deactivation-share-reason'                => __( 'If you have a moment, please let us know why you are deactivating',
			'freemius' ),
		'deactivation-modal-button-confirm'        => __( 'Yes - Deactivate', 'freemius' ),
		'deactivation-modal-button-submit'         => __( 'Submit & Deactivate', 'freemius' ),
		'cancel'                                   => __( 'Cancel', 'freemius' ),
		'reason-no-longer-needed'                  => __( 'I no longer need the plugin', 'freemius' ),
		'reason-found-a-better-plugin'             => __( 'I found a better plugin', 'freemius' ),
		'reason-needed-for-a-short-period'         => __( 'I only needed the plugin for a short period', 'freemius' ),
		'reason-broke-my-site'                     => __( 'The plugin broke my site', 'freemius' ),
		'reason-suddenly-stopped-working'          => __( 'The plugin suddenly stopped working', 'freemius' ),
		'reason-cant-pay-anymore'                  => __( "I can't pay for it anymore", 'freemius' ),
		'reason-temporary-deactivation'            => __( "It's a temporary deactivation. I'm just debugging an issue.",
			'freemius' ),
		'reason-other'                             => _x( 'Other',
			'the text of the "other" reason for deactivating the plugin that is shown in the modal box.', 'freemius' ),
		'ask-for-reason-message'                   => __( 'Kindly tell us the reason so we can improve.', 'freemius' ),
		'placeholder-plugin-name'                  => __( "What's the plugin's name?", 'freemius' ),
		'placeholder-comfortable-price'            => __( 'What price would you feel comfortable paying?', 'freemius' ),
		'reason-couldnt-make-it-work'              => __( "I couldn't understand how to make it work", 'freemius' ),
		'reason-great-but-need-specific-feature'   => __( "The plugin is great, but I need specific feature that you don't support",
			'freemius' ),
		'reason-not-working'                       => __( 'The plugin is not working', 'freemius' ),
		'reason-not-what-i-was-looking-for'        => __( "It's not what I was looking for", 'freemius' ),
		'reason-didnt-work-as-expected'            => __( "The plugin didn't work as expected", 'freemius' ),
		'placeholder-feature'                      => __( 'What feature?', 'freemius' ),
		'placeholder-share-what-didnt-work'        => __( "Kindly share what didn't work so we can fix it for future users...",
			'freemius' ),
		'placeholder-what-youve-been-looking-for'  => __( "What you've been looking for?", 'freemius' ),
		'placeholder-what-did-you-expect'          => __( "What did you expect?", 'freemius' ),
		'reason-didnt-work'                        => __( "The plugin didn't work", 'freemius' ),
		'reason-dont-like-to-share-my-information' => __( "I don't like to share my information with you", 'freemius' ),
		'dont-have-to-share-any-data'              => __( "You might have missed it, but you don't have to share any data and can just %s the opt-in.",
			'freemius' ),
		#endregion Plugin Deactivation

		#region Connect
		'hey-x'                                    => _x( 'Hey %s,', 'greeting', 'freemius' ),
		'thanks-x'                                 => _x( 'Thanks %s!', 'a greeting. E.g. Thanks John!', 'freemius' ),
		'connect-message'                          => __( 'Never miss an important update - opt-in to our security and feature updates notifications, and non-sensitive diagnostic tracking with %4$s.',
			'freemius' ),
		'connect-message_on-update'                => __( 'Please help us improve %1$s! If you opt-in, some data about your usage of %1$s will be sent to %4$s. I
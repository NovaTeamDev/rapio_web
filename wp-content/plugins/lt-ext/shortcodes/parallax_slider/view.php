<?php if ( ! defined( 'ABSPATH' ) ) die( 'Forbidden' );
/**
 * Zoom Slider Shortcode
 */

$args = get_query_var('like_sc_parallax_slider');

$class = '';
if ( !empty($args['class']) ) $class .= ' '. esc_attr($args['class']);
if ( !empty($args['id']) ) $id = ' id="'. esc_attr($args['id']). '"'; else $id = '';

$bg = ltx_get_attachment_img_url($atts['bg']);
$lt = ltx_get_attachment_img_url($atts['lt']);
$rt = ltx_get_attachment_img_url($atts['rt']);
$home = ltx_get_attachment_img_url($atts['home']);
$cat = ltx_get_attachment_img_url($atts['cat']);

?>
<div class="ltx-home-slider">

	<div class="ltx-parallax-slider">
		<div class="ltx-slider-inner"><?php echo do_shortcode( $content ); ?></div>
		<div data-depth="0.6" class="ltx-layer">
			<img class="ltx-left" alt="bg" src="<?php echo esc_url($lt[0]); ?>" >
		</div>

		<div data-depth="0.6" class="ltx-layer">
			<img class="ltx-right" alt="bg" src="<?php echo esc_url($rt[0]); ?>" >
		</div>		
	</div>	
</div>
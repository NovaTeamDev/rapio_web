<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */

$chaitan_sidebar_hidden = false;
$chaitan_layout = 'classic';

if ( function_exists( 'FW' ) ) {

	$chaitan_sidebar = 'right';
	$blog_wrap_class = 'col-lg-8 col-md-8 col-xs-12 matchHeight';

	$chaitan_sidebar = fw_get_db_settings_option( 'blog_list_sidebar' );
	$chaitan_layout = fw_get_db_settings_option( 'blog_layout' );

	if ( $chaitan_sidebar == 'hidden' ) $chaitan_sidebar_hidden = true;

	if ($chaitan_layout == 'three-cols') {

		$chaitan_sidebar_hidden = true;
	}

	if ( $chaitan_sidebar == 'left' ) $blog_wrap_class = 'col-xl-8 col-xl-push-4 col-lg-8 col-lg-push-4 col-lg-offset-0 col-md-12 col-xs-12 matchHeight';

	$blog_class = '';
	if ( $chaitan_layout == 'two-cols' OR $chaitan_layout == 'three-cols' ) {

		$blog_class = 'masonry';
		if ( $chaitan_sidebar_hidden ) $blog_wrap_class = 'col-lg-12 col-xs-12';
	}
		else {

		if ( $chaitan_sidebar_hidden ) $blog_wrap_class = 'col-xl-8 col-lg-10 col-md-12 col-xs-12';	
	}
}

get_header(); ?>
<div class="inner-page margin-default">
	<div class="row <?php if ( $chaitan_sidebar_hidden ) echo 'centered'; ?>">
        <div class="<?php echo esc_attr( $blog_wrap_class ); ?>" data-mh="ltx-col">
            <div class="blog blog-block layout-<?php echo esc_attr($chaitan_layout); ?>">
				<?php

				if ( $wp_query->have_posts() ) :

	            	echo '<div class="row">';
					while ( $wp_query->have_posts() ) : the_post();

						// Showing classic blog without framework
						if ( !function_exists( 'fw_get_db_settings_option' ) ) {

							get_template_part( 'tmpl/content-post-one-col' );
						}
							else {

							set_query_var( 'chaitan_layout', $chaitan_layout );

							if ($chaitan_layout == 'three-cols') {

								get_template_part( 'tmpl/content-post-three-cols' );
							}
								else
							if ($chaitan_layout == 'two-cols') {

								get_template_part( 'tmpl/content-post-two-cols' );
							}
								else {

								get_template_part( 'tmpl/content-post-one-col' );
							}
						}

					endwhile;
					echo '</div>';
				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'tmpl/content', 'none' );

				endif;

				?>
				<?php
				if ( have_posts() ) {

					chaitan_paging_nav();
				}
	            ?>
	        </div>
	    </div>
	    <?php
	    if ( !$chaitan_sidebar_hidden ) {

            if ( $chaitan_sidebar == 'left' ) {

            	get_sidebar( 'left' );
            }
            	else  {

            	get_sidebar();
            }
	    }
	    ?>
	</div>
</div>
<?php

get_footer();


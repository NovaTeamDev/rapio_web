<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}


$options = array(
	'main' => array(
		'title'   => 'LTX Post Format',
		'type'    => 'box',
		'options' => array(
			'featured'    => array(
				'label' => esc_html__( 'Featured Post', 'chaitan' ),
				'type'  => 'checkbox',
			),			
			'gallery'    => array(
				'label' => esc_html__( 'Gallery', 'chaitan' ),
				'desc' => esc_html__( 'Upload featured images for slider gallery post type', 'chaitan' ),
				'type'  => 'multi-upload',
			),				
		),
	),
);


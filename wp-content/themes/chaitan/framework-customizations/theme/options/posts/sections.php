<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}


$options = array(
	'theme_block' => array(
		'title'   => esc_html__( 'Theme Block', 'chaitan' ),
		'label'   => esc_html__( 'Theme Block', 'chaitan' ),
		'type'    => 'select',
		'choices' => array(
			'none'  => esc_html__( 'Not Assigned', 'chaitan' ),
			'subscribe'  => esc_html__( 'Subscribe', 'chaitan' ),
			'top_bar'  => esc_html__( 'Top Bar', 'chaitan' ),
		),
		'value' => 'none',
	)
);



<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'main' => array(
		'title'   => false,
		'type'    => 'box',
		'options' => array(
			'subheader'    => array(
				'label' => esc_html__( 'Subheader', 'chaitan' ),
				'type'  => 'text',
			),
			'rate'    => array(
				'type'    => 'select',
				'label' => esc_html__( 'Rate', 'chaitan' ),				
				'description'   => esc_html__( 'Null for hidden', 'chaitan' ),
				'choices' => array(
					0,1,2,3,4,5
				),
			),						
		),
	),		
);


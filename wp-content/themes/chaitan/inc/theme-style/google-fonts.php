<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Including google fonts
 */

$chaitan_font_main = fw_get_db_settings_option( 'font-text' );
$chaitan_font_headers = fw_get_db_settings_option( 'font-headers' );

$chaitan_font_main_weights = fw_get_db_settings_option( 'font-text-weights' );
$chaitan_font_headers_weights = fw_get_db_settings_option( 'font-headers-weights' );

$chaitan_font_main['variation'] = str_replace('regular', '400', $chaitan_font_main['variation']);
$chaitan_font_headers['variation'] = str_replace('regular', '400', $chaitan_font_headers['variation']);

$chaitan_google_fonts = array();
$chaitan_google_fonts[$chaitan_font_main['family']][$chaitan_font_main['variation']] = true;
$chaitan_google_fonts[$chaitan_font_headers['family']][$chaitan_font_headers['variation']] = true;

if ( !empty($chaitan_font_main_weights) ) {

	$chaitan_items = explode(',', $chaitan_font_main_weights);
	foreach ( $chaitan_items as $item) $chaitan_google_fonts[$chaitan_font_main['family']][$item] = true;
}

if ( !empty($chaitan_font_headers_weights) ) {

	$chaitan_items = explode(',', $chaitan_font_headers_weights);
	foreach ( $chaitan_items as $item) $chaitan_google_fonts[$chaitan_font_headers['family']][$item] = true;
}

$chaitan_google_subsets[$chaitan_font_main['subset']] = true;
$chaitan_google_subsets[$chaitan_font_headers['subset']] = true;

$css['font_main'] = esc_attr($chaitan_font_main['family']);
$css['font_headers'] = esc_attr($chaitan_font_headers['family']);

$family = $subset = '';
foreach ( $chaitan_google_fonts as $font => $styles ) {

	if ( !empty($family) ) $family .= "%7C";
    $family .= str_replace( ' ', '+', $font ) . ':' . implode( ',', array_keys($styles) );
}

foreach ( $chaitan_google_subsets as $subset_ => $val ) {

	if ( !empty($subset) ) $subset .= ",";
    if ( !empty($subset_) ) $subset .= $subset_;
}

$query_args = array( 'family' => $family, 'subset' => $subset );
wp_enqueue_style( 'chaitan_google_fonts', esc_url( add_query_arg( $query_args, '//fonts.googleapis.com/css' ) ), array(), null );



<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * TGM Plugin Activation
 */

require_once get_template_directory() . '/tgm-plugin-activation/class-tgm-plugin-activation.php';

if ( !function_exists('chaitan_action_theme_register_required_plugins') ) {

	function chaitan_action_theme_register_required_plugins() {

		$config = array(

			'id'           => 'chaitan',
			'menu'         => 'chaitan-install-plugins',
			'parent_slug'  => 'themes.php',
			'capability'   => 'edit_theme_options',
			'has_notices'  => true,
			'dismissable'  => true,
			'is_automatic' => false,
		);

		tgmpa( array(

			array(
				'name'      => esc_html__('Unyson', 'chaitan'),
				'slug'      => 'unyson',
				'required'  => true,
			),
			array(
				'name'      => esc_html__('LT Extension', 'chaitan'),
				'slug'      => 'lt-ext',
				'source'   	=> get_template_directory() . '/inc/plugins/lt-ext.zip',
				'version'   => '1.9.7',
				'required'  => true,
			),
			array(
				'name'      => esc_html__('WPBakery Page Builder', 'chaitan'),
				'slug'      => 'js_composer',
				'source'   	=> 'http://updates.like-themes.com/plugins/js_composer.zip',
				'required'  => true,
			),		
			array(
				'name'      => esc_html__('Envato Market', 'chaitan'),
				'slug'      => 'envato-market',
				'source'   	=> get_template_directory() . '/inc/plugins/envato-market.zip',
				'required'  => false,
			),													
			array(
				'name'      => esc_html__('Breadcrumb-navxt', 'chaitan'),
				'slug'      => 'breadcrumb-navxt',
				'required'  => false,
			),
			array(
				'name'      => esc_html__('Contact Form 7', 'chaitan'),
				'slug'      => 'contact-form-7',
				'required'  => true,
			),
			array(
				'name'      => esc_html__('The Events Calendar', 'chaitan'),
				'slug'      => 'the-events-calendar',
				'required'  => false,
			),			
			array(
				'name'      => esc_html__('WP Instagram Widget', 'chaitan'),
				'slug'      => 'wp-instagram-widget',
				'required'  => false,
			),	
			array(
				'name'      => esc_html__('Post-views-counter', 'chaitan'),
				'slug'      => 'post-views-counter',
				'required'  => false,
			),
			array(
				'name'       => esc_html__('MailChimp for WordPress', 'chaitan'),
				'slug'       => 'mailchimp-for-wp',
				'required'   => false,
			),		
			array(
				'name'       => esc_html__('WooCommerce', 'chaitan'),
				'slug'       => 'woocommerce',
				'required'   => false,
			),
			array(
				'name'      => esc_html__('Simple Share Buttons Adder', 'chaitan'),
				'slug'      => 'simple-share-buttons-adder',
				'required'  => false,
			),
			array(
				'name'      => esc_html__('User Profile Picture', 'chaitan'),
				'slug'      => 'metronet-profile-picture',
				'required'  => false,
			),								
		), $config);
	}
}
add_action( 'tgmpa_register', 'chaitan_action_theme_register_required_plugins' );

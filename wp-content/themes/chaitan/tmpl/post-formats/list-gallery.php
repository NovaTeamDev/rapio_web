<?php

/**

 * Gallery post format

 */



$post_class = '';

if ( function_exists( 'FW' ) ) {



	$gallery_files = fw_get_db_post_option(get_The_ID(), 'gallery');

}



?>

<article id="post-<?php the_ID(); ?>" <?php post_class( esc_attr($post_class) ); ?>>

	<?php

		if ( !empty( $gallery_files ) ) {



			echo '

			<div class="swiper-container ltx-post-gallery" data-autoplay="4000">

				<div class="swiper-wrapper">';



			foreach ( $gallery_files as $item ) {



				echo '<a href="'.esc_url(get_the_permalink()).'" class="swiper-slide">';

					echo wp_get_attachment_image( $item['attachment_id'], 'chaitan-featured' );

				echo '</a>';

			}



			echo '</div>

				<div class="arrows">

					<a href="#" class="arrow-left fa fa-arrow-left"></a>

					<a href="#" class="arrow-right fa fa-arrow-right"></a>

				</div>

				<div class="swiper-pages"></div>

			</div>';

		}

			else

		if ( has_post_thumbnail() ) {



			$chaitan_photo_class = 'photo';



		    echo '<a href="'.esc_url(get_the_permalink()).'" class="'.esc_attr($chaitan_photo_class).'">';



		    the_post_thumbnail();



		    echo '</a>';

		}

	?>

    <div class="description">

		<div class="blog-info top">

			<?php

           		echo '<a href="'. esc_url( get_the_permalink() ) .'" class="date">'.get_the_date().'</a>';

			?>

	    </div>    

        <a href="<?php esc_url( the_permalink() ); ?>" class="header"><h4><?php the_title(); ?></h4></a>

        <div class="text text-page">

			<?php

				add_filter( 'the_content', 'chaitan_excerpt' );

			    if( strpos( $post->post_content, '<!--more-->' ) ) {



			        the_content( esc_html__( 'Read more', 'chaitan' ) );

			    }	

			?>

        </div>

			      

    </div>    

</article>


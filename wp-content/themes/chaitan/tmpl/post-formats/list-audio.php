<?php
/**
 * Audio Post Format
 */

$post_class = '';

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( esc_attr($post_class) ); ?>>
	<div class="ltx-wrapper">
		<?php

		if ( has_post_thumbnail() ) {

			$chaitan_photo_class = 'photo';

		    echo '<a href="'.esc_url(get_the_permalink()).'" class="'.esc_attr($chaitan_photo_class).'">';

			    the_post_thumbnail();

		    echo '</a>';
		}

		$mp3 = chaitan_find_http(get_the_content());

		echo wp_audio_shortcode(
			array('src'	=>	esc_url($mp3))
		);

		?>
	</div>
    <div class="description">
		<div class="blog-info top">
			<?php
           		echo '<a href="'. esc_url( get_the_permalink() ) .'" class="date">'.get_the_date().'</a>';
			?>	
	    </div>    
        <a href="<?php esc_url( the_permalink() ); ?>" class="header"><h4><?php the_title(); ?></h4></a> 
    </div>   	
</article>
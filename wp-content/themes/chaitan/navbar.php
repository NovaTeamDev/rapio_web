<?php

/**

 * Navigation Bar

 */

$navbar_layout = 'desktop';

$basket_icon = 'disabled';

$navbar_logo = $navlogo_class = $navbar_affix = '';

$navbar_container = 'container';

$navbar_class = 'navbar-collapse collapse';

$navbar_mobile_width = '1199';



if ( function_exists( 'FW' ) ) {



	$basket_icon = fw_get_db_settings_option( 'basket-icon' );

	$navbar_layout = fw_get_db_post_option( $wp_query->get_queried_object_id(), 'navbar-layout' );

	$navbar_affix = fw_get_db_settings_option( 'navbar-affix' );



	if ( empty( $navbar_layout ) OR $navbar_layout == 'transparent' OR $navbar_layout == 'default' ) {



		$navbar_layout = 'desktop';

	}



	if ( $navbar_layout == 'desktop-center-absolute' ) {



		$navbar_logo = 'center';

	}

		else

	if ( $navbar_layout == 'white' ) {



		$navbar_logo = 'dark';

	}



	if ($navbar_affix == 'affix' AND ( $navbar_layout == 'desktop' OR $navbar_layout == 'white' )) {



		$navbar_affix = 'affix';

	}

		else {



		$navbar_affix = '';

	}

}



if ( empty($basket_icon) ) {



	$basket_icon = 'disabled';

}



if ( $navbar_layout != 'disabled' ):



chaitan_the_topbar_block();



if ( !function_exists( 'FW' ) ) {



	$navbar_layout .= ' navbar-simple ';

}



?>

<div id="nav-wrapper" class="navbar-layout-<?php echo esc_attr($navbar_layout);?>">

	<nav class="navbar" data-spy="<?php echo esc_attr($navbar_affix); ?>" data-offset-top="35">

		<div class=" <?php echo esc_attr($navbar_container); ?>">

			<div class="navbar-logo <?php echo esc_attr($navlogo_class); ?>">	

				<?php

					chaitan_the_logo($navbar_logo);

				?>

			</div>

			<div id="navbar" class="<?php echo esc_attr( $navbar_class ); ?>" data-mobile-screen-width="<?php echo esc_attr( $navbar_mobile_width ); ?>">

				<div class="toggle-wrap">

					<?php

						chaitan_the_logo();

					?>						

					<button type="button" class="navbar-toggle collapsed">

						<span class="close"><span class="fa fa-close"></span></span>

					</button>							

					<div class="clearfix"></div>

				</div>

				<?php

					wp_nav_menu(array(



						'theme_location'	=> 'primary',

						'menu_class' 		=> 'nav navbar-nav',

						'container'			=> 'ul',

						'link_before' 		=> '<span>',     

						'link_after'  		=> '</span>'							

					));



					chaitan_the_navbar_icons( $navbar_layout );

				?>

				<div class="mobile-controls">

					<?php

						chaitan_the_navbar_icons( $navbar_layout, true );

					?>

				</div>				

			</div>

			<div class="navbar-controls">	

				<button type="button" class="navbar-toggle collapsed">

					<span class="icon-bar top-bar"></span>

					<span class="icon-bar middle-bar"></span>

					<span class="icon-bar bottom-bar"></span>

				</button>

				<?php

					chaitan_the_navbar_icons( 'basket-only' );

				?>				

			</div>

		</div>

	</nav>

</div>

<?php



endif;




<?php
/*
Template Name: ordina-online
*/
?>
<?php 
session_start();

get_header(); 
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/shortcodes-ultimate/assets/css/box-shortcodes.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-admin/css/color-picker.min.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/contact-form-7/includes/css/styles.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/revslider/rs-plugin/css/settings.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/revslider/rs-plugin/css/static-captions.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/siteorigin-panels/css/front.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/advanced-responsive-video-embedder/public/assets/css/public.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/themes/organique/assets/stylesheets/bootstrap.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/themes/organique/assets/stylesheets/main.css');
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/woocommerce/assets/css/prettyPhoto.css');



$stringa = md5(microtime());
$risultato = substr($stringa, 0, 5);
?>

<?php
if(isset($_POST['your-name'])) {

			$messaggio = "&Egrave; stata effettuata una nuova richiesta online.<br/>
							Di seguito i dettagli:<br/>
							<b>Nome Cognome</b>: ". $_POST['your-name'] ."<br/>
							<b>Email</b>: ". $_POST['your-email']."<br/>
							<b>Indirizzo</b>: ". $_POST['your-address']."<br/>
							<b>CAP</b>: ". $_POST['your-cap']."<br/>
							<b>Comune</b>: ". $_POST['your-city']."<br/>
							<b>Metodo di pagamento</b>: ". $_POST['your-payment']."<br/><br/>
							<b>Prodotti:</b>";
							
			if($_POST['qt1'] != 0)	
				$messaggio .= "<b>Carnaroli 1 Kg.:</b>".$_POST['qt1']."<br/>";
			if($_POST['qt2'] != 0)	
				$messaggio .= "<b>Carnaroli 5 Kg.:</b>".$_POST['qt2']."<br/>";
			if($_POST['qt3'] != 0)	
				$messaggio .= "<b>Baldo 1 Kg.:</b>".$_POST['qt3']."<br/>";
			if($_POST['qt4'] != 0)	
				$messaggio .= "<b>Roma 1 Kg.:</b>".$_POST['qt4']."<br/>";
			if($_POST['qt5'] != 0)	
				$messaggio .= "<b>Vialone Nano 1 Kg.:</b>".$_POST['qt5']."<br/>";
			if($_POST['qt6'] != 0)	
				$messaggio .= "<b>Carnaroli Extra 1 Kg.:</b>".$_POST['qt6']."<br/>";
			if($_POST['qt7'] != 0)	
				$messaggio .= "<b>Integrale 1 Kg.:</b>".$_POST['qt7']."<br/>";
			if($_POST['qt8'] != 0)	
				$messaggio .= "<b>Farina 500 gr.:</b>".$_POST['qt8']."<br/>";
			if($_POST['qt9'] != 0)	
				$messaggio .= "<b>Farina 4 Kg.:</b>".$_POST['qt9']."<br/>";
			if($_POST['qt10'] != 0)	
				$messaggio .= "<b>Farina integrale 4 Kg.:</b>".$_POST['qt10']."<br/>";
			if($_POST['qt11'] != 0)	
				$messaggio .= "<b>Carnaroli al funghi 500 gr.:</b>".$_POST['qt11']."<br/>";			
			if($_POST['qt12'] != 0)	
				$messaggio .= "<b>Carnaroli 1 Kg., pacco da 10 pz.:</b>".$_POST['qt12']."<br/>";			
			if($_POST['qt13'] != 0)	
				$messaggio .= "<b>Baldo 1 Kg., pacco da 10 pz.:</b>".$_POST['qt13']."<br/>";					
			if($_POST['qt14'] != 0)	
				$messaggio .= "<b>Roma 1 Kg., pacco da 10 pz.:</b>".$_POST['qt14']."<br/>";	
				
			$header = "From: info@risorapio.it\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: text/html; charset=UTF-8\r\n";
			$oggetto = "Ordine Online";
			mail('davide.petteno.82@gmail.com',$oggetto,$messaggio,$header);		
			

			$messaggio2 = "Gentile ". $_POST['your-name'] ."
							la ringraziamo per aver richiesto il preventivo online.<br/>
							Sar&agrave; ricontattato nel pi&ugrave; breve tempo possibile";			


			mail($_POST['your-email'],$oggetto,$messaggio2,$header);				
	
?>
<div class="container push-down-60">
	<div class="row">
		<div class="col-xs-12" role="main">
			<article class="post-59 page type-page status-publish hentry">
				<header class="post-title center">
				<h1>Ordina Online</h1>
				<hr class="divider">
				<div class="text-shrink">
					<p class="text-highlight">Email ricevuta</p>
				</div>
				<hr class="divider divider-about">
				</header>
			</article>
		</div>
	</div>
	<div class="blog-content__text">
		<div class="su-row">
			<div class="su-column su-column-size-3-4">	
			</div>
			<div class="su-column su-column-size-1-4">
				<div class="su-column-inner su-clearfix">
					<h2>
						<span class="light">Azienda Agricola</span>
						 Rapio
					</h2>
					<p>
						<strong>
						via Gramsci, 9
						<br>
						28079 Vespolate (NO)
						</strong>
					</p>
					<p>
						<strong>+39 0321 882227</strong>
						<br>
						<strong>
						<a href="mailto:info@risorapio.it">info@risorapio.it</a>
						</strong>
					</p>
				</div>
			</div>	
		</div>
	</div>
</div>
<?php	
}
else {
?>

<script>
jQuery(document).ready(function () {
  jQuery(".numeric").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });
});

function verificaRiso() {
	var totale_riso = 0;
	totale_riso += jQuery('#qt1').val() * 1 ;
	totale_riso += jQuery('#qt3').val() * 1 ;
	totale_riso += jQuery('#qt4').val() * 1 ;
	totale_riso += jQuery('#qt5').val() * 1 ;
	totale_riso += jQuery('#qt6').val() * 1 ;
	totale_riso += jQuery('#qt7').val() * 1 ;

	jQuery('#riso1').empty();
	jQuery('#riso1').append(totale_riso);
}

function verificaFarina() {
	var totale_farina = 0;
	totale_farina += jQuery('#qt8').val() * 1 ;

	jQuery('#farina1').empty();
	jQuery('#farina1').append(totale_farina);
}

function verificaFunghi() {
	var totale_funghi = 0;
	totale_funghi += jQuery('#qt11').val() * 1 ;

	jQuery('#funghi1').empty();
	jQuery('#funghi1').append(totale_funghi);
}

function calculate() {

	var totale = 0;
	
	jQuery('#tot1').empty();
	var sconto1 = jQuery('#qt1').val() % 10;
	if(sconto1 != 0) {
		var tot1 = jQuery('#qt1').val() * 3;
		jQuery('#tot1').append(tot1.toFixed(2) +" &euro;");
		totale += jQuery('#qt1').val() * 3;
	}
	else {
		var calcola1 = jQuery('#qt1').val() / 10;
		var tot1 = calcola1 * 27;
		jQuery('#tot1').append(tot1.toFixed(2) +" &euro;");
		totale += tot1;
	}
	
	totale += jQuery('#qt2').val() * 13.50 * 3;
	var tot2 = jQuery('#qt2').val() * 13.50 * 3;
	jQuery('#tot2').empty();
	jQuery('#tot2').append(tot2.toFixed(2) +" &euro;");
	
	jQuery('#tot3').empty();
	var sconto3 = jQuery('#qt3').val() % 10;
	if(sconto3 != 0) {
		var tot3 = jQuery('#qt3').val() * 2.30;
		jQuery('#tot3').append(tot3.toFixed(2) +" &euro;");
		totale += jQuery('#qt3').val() * 2.30;
	}
	else {
		var calcola3 = jQuery('#qt3').val() / 10;
		var tot3 = calcola3 * 21;
		jQuery('#tot3').append(tot3.toFixed(2) +" &euro;");
		totale += tot3;
	}	
	
	jQuery('#tot4').empty();
	var sconto4 = jQuery('#qt4').val() % 10;
	if(sconto4 != 0) {
		var tot4 = jQuery('#qt3').val() * 2.30;
		jQuery('#tot4').append(tot4.toFixed(2) +" &euro;");
		totale += jQuery('#qt4').val() * 2.30;
	}
	else {
		var calcola4 = jQuery('#qt4').val() / 10;
		var tot4 = calcola4 * 21;
		jQuery('#tot4').append(tot4.toFixed(2) +" &euro;");
		totale += tot4;
	}	
	
	totale += jQuery('#qt5').val() * 3;
	var tot5 = jQuery('#qt5').val() * 3;
	jQuery('#tot5').empty();
	jQuery('#tot5').append(tot5.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt6').val() * 3.50;
	var tot6 = jQuery('#qt6').val() * 3.50;
	jQuery('#tot6').empty();
	jQuery('#tot6').append(tot6.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt7').val() * 2.50;
	var tot7 = jQuery('#qt7').val() * 2.50;
	jQuery('#tot7').empty();
	jQuery('#tot7').append(tot7.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt8').val() * 1.20 * 20;
	var tot8 = jQuery('#qt8').val() * 1.20 * 20;
	jQuery('#tot8').empty();
	jQuery('#tot8').append(tot8.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt9').val() * 6 * 3;
	var tot9 = jQuery('#qt9').val() * 6 * 3;
	jQuery('#tot9').empty();
	jQuery('#tot9').append(tot9.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt10').val() * 6 * 3;
	var tot10 = jQuery('#qt10').val() * 6 * 3;
	jQuery('#tot10').empty();
	jQuery('#tot10').append(tot10.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt11').val() * 5 * 6;
	var tot11 = jQuery('#qt11').val() * 5 * 6;
	jQuery('#tot11').empty();
	jQuery('#tot11').append(tot11.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt12').val() * 27;
	var tot12 = jQuery('#qt12').val() * 27;
	jQuery('#tot12').empty();
	jQuery('#tot12').append(tot12.toFixed(2) +" &euro;");		
	
	totale += jQuery('#qt13').val() * 21;
	var tot13 = jQuery('#qt13').val() * 21;
	jQuery('#tot13').empty();
	jQuery('#tot13').append(tot13.toFixed(2) +" &euro;");		

	totale += jQuery('#qt14').val() * 21;
	var tot14 = jQuery('#qt14').val() * 21;
	jQuery('#tot14').empty();
	jQuery('#tot14').append(tot14.toFixed(2) +" &euro;");	
	
	jQuery('#euro').empty();
	jQuery('#euro').append(totale.toFixed(2)+" &euro;");
}

function checkForm() {
	var check = true;

	jQuery('#error_riso').empty();
	jQuery('#error_farina').empty();
	jQuery('#error_name').empty();
	jQuery('#error_cap').empty();
	jQuery('#error_email').empty();
	jQuery('#error_address').empty();
	jQuery('#error_city').empty();
	jQuery('#error_euro').empty();
	jQuery('#error_captcha').empty();
	
	if(jQuery('#your-name').val() == ''){
		jQuery('#error_name').append('<font color="red">Compilare il campo</font>');
		check =  false;
	}
	if(jQuery('#your-cap').val() == ''){
		jQuery('#error_cap').append('<font color="red">Compilare il campo</font>');
		check =  false;
	}
	if(jQuery('#your-email').val() == ''){
		jQuery('#error_email').append('<font color="red">Compilare il campo</font>');
		check =  false;
	}
	if(jQuery('#your-address').val() == ''){
		jQuery('#error_address').append('<font color="red">Compilare il campo</font>');
		check =  false;
	}
	if(jQuery('#your-city').val() == ''){
		jQuery('#error_city').append('<font color="red">Compilare il campo</font>');
		check =  false;
	}	

	if(jQuery('#cpt').val() != '<?php echo $risultato;?>'){
		jQuery('#error_captcha').append('<font color="red"><strong>Codice errato</strong></font>');
		check =  false;
	}	
	
	var remainder = jQuery('#riso1').text() % 10;
	var remainder2 = jQuery('#farina1').text() % 20;
	
	if(jQuery('#euro').text() == 0){
		jQuery('#error_euro').append('<font color="red"><strong>Selezionare almeno un prodotto</strong></font>');
		check =  false;
	}
	if(remainder != 0) {
		jQuery('#error_riso').append('<font color="red"><strong>Le confezioni devono essere multipli di 10</strong></font>');
		check =  false;
	}
	if(remainder2 != 0) {
		jQuery('#error_farina').append('<font color="red"><strong>Le confezioni devono essere multipli di 20</strong></font>');
		check =  false;
	}	
	
return check;	


}
</script>

<div class="container push-down-60">
	<div class="row">
		<div class="col-xs-12" role="main">
			<article class="post-59 page type-page status-publish hentry">
				<header class="post-title center">
				<h1>Ordina Online</h1>
				<hr class="divider">
				<div class="text-shrink">
					<p class="text-highlight">Compila il modulo e richiedi con pochi click il preventivo. Calcoleremo le spese di spedizione secondo le tue esigenze e potrai ricevere in brevissimo tempo il nostro riso direttamente a casa tua!<br/><font style="text-transform:uppercase"; color="#006700">I nostri imballaggi contengono 10 confezioni di riso da 1kg o 20 di farina da 500gr. Componi il tuo ordine e richiedi un preventivo!</font></p>
				</div>
				<hr class="divider divider-about">
				</header>
				<div class="blog-content__text">
					<div class="su-row">
						<div class="su-column su-column-size-3-4">
							<div class="su-column-inner su-clearfix">
								<div>
									<div class="screen-reader-response"></div>
									<form class="" method="post" action="" onsubmit="return checkForm()" name="">
										<div style="display: none;">
										</div>
												<p>
												<span class="hidden-xs">
													I campi contrassegnati con
													<span class="warning">*</span>
													sono obbligatori
												</span>
												</p>								
										<div class="row">
	
											<div class="col-xs-12 col-sm-6">
												<div class="form-group">
													<label class="text-dark" for="name">
														Nome e Cognome
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text" type="text" size="40" value="" name="your-name" id="your-name">
													</span>
													<div id="error_name"></div>
												</div>
												<div class="form-group">
													<label class="text-dark" for="cap">
														CAP
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text" type="text" size="40" value="" name="your-cap" id="your-cap">
													</span>
													<div id="error_cap"></div>
												</div>
												<div class="form-group">
													<label class="text-dark" for="email">
													Email
													<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-email">
														<input class="wpcf7-form-control wpcf7-text" type="email" size="40" value="" name="your-email" id="your-email">
													</span>
													<div id="error_email"></div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="form-group">
													<label class="text-dark" for="name">
														Indirizzo
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text" type="text" size="40" value="" name="your-address" id="your-address">
													</span>
													<div id="error_address"></div>
												</div>
												<div class="form-group">
													<label class="text-dark" for="name">
														Comune
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text" type="text" size="40" value="" name="your-city" id="your-city">
													</span>
													<div id="error_city"></div>
												</div>
                                                   <div class="form-group">
														<label class="text-dark" for="name">
															Metodo di pagamento
															<span class="warning">*</span>
														</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<select id="your-payment" name="your-payment">
															<option value="bonifico">Bonifico all'ordine</option>
															<option value="contrassegno">Contrassegno ( maggiorazione di 6 &euro; )</option>
														</select>
													</span>													
											  </div>
										  </div>												
									  </div>
											
											
											<div class="col-xs-12 col-sm">
												<div class="form-group">
                                                    <table width="100%" border="1" bordercolor="#CCCCCC" style="padding:5px">
														<tr>
															<th width="20%" height="40px" style="padding:5px" bgcolor="#006700"><font style="color:#FFF; text-transform:uppercase;">Prodotto</font></td>
															<th width="35%" height="40px" style="padding:5px" bgcolor="#006700"><font style="color:#FFF; text-transform:uppercase;">Confezione</font></td>
															<th width="25%" height="40px" style="padding:5px" bgcolor="#006700"><font style="color:#FFF; text-transform:uppercase;">Costo a confezione</font></td>
															<th width="10%" height="40px" style="padding:5px" bgcolor="#006700"><font style="color:#FFF; text-transform:uppercase;">Quantita'</font></td>
															<th width="10%" height="40px" style="padding:5px" bgcolor="#006700"><font style="color:#FFF; text-transform:uppercase;">Totale</font></td>
														</tr>
														<tr>
															<td width="10%" rowspan="3" style="padding-left:5px"><img class=" size-full wp-image-301 alignleft" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/carnaroli-icona.png" alt="carnaroli-icona" width="40" height="37" /> <span style="color: #5a2920;"><strong>Carnaroli</strong></span></td>
															<td style="padding-left:5px">1 Kg sotto vuoto</td>
															<td style="padding-left:5px">&euro; 3.00</td>
															<td style="padding-left:5px"><input type="text" name="qt1" id="qt1" value="0" class="numeric" onkeyup="verificaRiso();calculate()" size="4" /></td>
															<td style="padding-left:5px"><div id="tot1">0.00 &euro;</div></td>
														</tr>
														<tr>
															<td style="padding-left:5px">1 Kg sotto vuoto, cartone da 10 pz.</td>
															<td style="padding-left:5px">&euro; 27.00 <strong>- sconto 10%</strong></font></td>
															<td style="padding-left:5px"><input type="text" name="qt12" id="qt12" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot12">0.00 &euro;</div></td>
														</tr>															
														<tr>
															<td style="padding-left:5px">5 Kg sotto vuoto, cartone da 3 pz.<br/><font size="-1">- su prenotazione -</font></td>
															<td style="padding-left:5px">&euro; 13.50</td>
															<td style="padding-left:5px"><input type="text" name="qt2" id="qt2" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot2">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%" rowspan="2" style="padding-left:5px"><img class="alignleft size-full wp-image-299" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/baldo-icona.png" alt="baldo-icona" width="40" height="37" />
																<span style="color: #843e37;"><strong>Baldo</strong></span></p>
															</td>
															<td style="padding-left:5px">1 Kg sotto vuoto</td>
															<td style="padding-left:5px">&euro; 2.30</td>
															<td style="padding-left:5px"><input type="text" name="qt3" id="qt3" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot3">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td style="padding-left:5px">1 Kg sotto vuoto, cartone da 10 pz.</td>
															<td style="padding-left:5px">&euro; 21.00 <strong>- sconto 9%</strong></td>
															<td style="padding-left:5px"><input type="text" name="qt13" id="qt13" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot13">0.00 &euro;</div></td>
														</tr>															
														<tr>
															<td width="20%" rowspan="2" style="padding-left:5px"><img class="alignleft size-full wp-image-298" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/roma-icona.png" alt="roma-icona" width="40" height="37" />
																<span style="color: #2b4625;"><strong>Roma</strong></span>
															</td>
															<td style="padding-left:5px">1 Kg sotto vuoto</td>
															<td style="padding-left:5px">&euro; 2.30</td>
															<td style="padding-left:5px"><input type="text" name="qt4" id="qt4" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot4">0.00 &euro;</div></td>
														</tr>
															
														<tr>
															<td style="padding-left:5px">1 Kg sotto vuoto, cartone da 10 pz.</td>
															<td style="padding-left:5px">&euro; 21.00 <strong>- sconto 9%</strong></td>
															<td style="padding-left:5px"><input type="text" name="qt14" id="qt14" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot14">0.00 &euro;</div></td>
														</tr>															
														<tr>
															<td style="padding-left:5px"><img class="alignleft size-full wp-image-300" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/vialone-icona.png" alt="vialone-icona" width="40" height="37" />
																<span style="color: #6f5e1d;"><strong>Vialone Nano</strong></span>
															</td>
															<td style="padding-left:5px">1 Kg sotto vuoto</td>
															<td style="padding-left:5px">&euro; 3.00</td>
															<td style="padding-left:5px"><input type="text" name="qt5" id="qt5" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot5">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td style="padding-left:5px"><img class=" size-full wp-image-301 alignleft" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/carnaroli-icona.png" alt="carnaroli-icona" width="40" height="37" /> <span style="color: #5a2920;"><strong>Carnaroli Extra</strong></span></td>
															<td style="padding-left:5px">1 Kg sotto vuoto</td>
															<td style="padding-left:5px">&euro; 3.50</td>
															<td style="padding-left:5px"><input type="text" name="qt6" id="qt6" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot6">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td style="padding-left:5px"><img class="alignleft size-full wp-image-297" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/integrale-icona.png" alt="integrale-icona" width="40" height="37" />
<span style="color: #e6882c;"><strong> Integrale</strong></span></td>
															<td style="padding-left:5px">1 Kg sotto vuoto</td>
															<td style="padding-left:5px">&euro; 2.50</td>
															<td style="padding-left:5px"><input type="text" name="qt7" id="qt7" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot7">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%" rowspan="2" style="padding-left:5px"><img class="alignleft size-full wp-image-297" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/farina-icona.png" alt="integrale-icona" width="40" height="37" />
<span style="color: #e1cb38;"><strong> Farina di mais</strong></span></td>
															<td style="padding-left:5px">500 gr. sotto vuoto, cartone da 20pz.</td>
															<td style="padding-left:5px">&euro; 1.20</td>
															<td style="padding-left:5px"><input type="text" name="qt8" id="qt8" value="0" onkeyup="verificaFarina();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot8">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td style="padding-left:5px">4 kg sotto vuoto, cartone da 3pz.</td>
															<td style="padding-left:5px">&euro; 6.00</td>
															<td style="padding-left:5px"><input type="text" name="qt9" id="qt9" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot9">0.00 &euro;</div></td>
														</tr>
														<tr>
															<td width="20%" rowspan="2" style="padding-left:5px"><img class="alignleft size-full wp-image-297" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/farina-icona.png" alt="integrale-icona" width="40" height="37" />
<span style="color: #e1cb38;"><strong> Farina di mais integrale</strong></span></td>
<td style="padding-left:5px">500 gr. sotto vuoto, cartone da 20pz.</td>
															<td style="padding-left:5px">&euro; 1.20</td>
															<td style="padding-left:5px"><input type="text" name="qt8" id="qt8" value="0" onkeyup="verificaFarina();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot8">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td style="padding-left:5px">4 kg sotto vuoto, cartone da 3pz.</td>
															<td style="padding-left:5px">&euro; 6.00</td>
															<td style="padding-left:5px"><input type="text" name="qt10" id="qt10" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot10">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td style="padding-left:5px"><img class="alignleft size-full wp-image-297" src="http://www.risorapio.it/new/wp-content/uploads/2015/01/funghi-icona.png" alt="integrale-icona" width="40" height="37" />
<span style="color: #b2ac9c;"><strong> Carnaroli ai funghi porcini</strong></span></td>
															<td style="padding-left:5px">500 gr., cartone da 6pz.</td>
															<td style="padding-left:5px">&euro; 5.00</td>
															<td style="padding-left:5px"><input type="text" name="qt11" id="qt11" value="0" onkeyup="verificaFunghi();calculate()" class="numeric" size="4" /></td>
															<td style="padding-left:5px"><div id="tot11">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td></td>
															<td width="40%" colspan="2" align="right" style="padding:5px"><strong>Totale confezioni di riso da 1 Kg.</strong></td>
															<td style="padding-left:5px"><div id="riso1">0</div></td>
															<td></td>
														</tr>	
														<tr>
															<td></td>
															<td width="40%" colspan="2" align="center"><div id="error_riso"></div></td>
															<td></td>
															<td></td>
														</tr>															
														<tr>
															<td></td>
															<td width="40%" colspan="2" align="right" style="padding:5px"><strong>Totale confezioni di farina da 500 gr.</strong></td>
															<td style="padding-left:5px"><div id="farina1">0</div></td>
															<td></td>
														</tr>	
														<tr>
															<td></td>
															<td width="40%" colspan="2" align="center"><div id="error_farina"></div></td>
															<td></td>
															<td></td>
														</tr>
                                                        <tr>
															<td></td>
															<td width="40%" colspan="2" align="right" style="padding:5px"><strong>Totale cartoni da 6pz. di riso ai funghi  da 500 gr.</strong></td>
															<td style="padding-left:5px"><div id="funghi1">0</div></td>
															<td></td>
														</tr>	
														<tr>
															<td></td>
															<td width="40%" colspan="2" align="center"><div id="error_riso"></div></td>
															<td></td>
															<td></td>
														</tr>														
														<tr>
															<td></td>
															<td width="40%" colspan="2" align="right" style="padding:5px"><strong>Totale (escluse spese di spedizione)</strong></td>
															<td></td>
															<td style="padding-left:5px"><div id="euro">0</div></td>
														</tr>	
														<tr>
															<td></td>
															<td width="40%" colspan="2" align="center"><div id="error_euro"></div></td>
															<td></td>
															<td></td>
														</tr>															
													</table>
                                                   </div>	 
												</div>	
												<div class="right">
												Inserisci il testo visualizzato <input type="text" id="cpt" name="cpt" value="" size="4"/><img src="/new/wp-content/themes/organique/captcha.php?risultato=<?php echo $risultato; ?>">
												</div>	
												<div id="error_captcha"></div>
												<br/>
												<div class="right">
													<input class="btn btn-warning" type="submit" value="Richiedi il preventivo con spese di spedizione incluse">
												</div>
												<p></p>
									</form>
								</div>
							</div>
						</div>
					<div class="su-column su-column-size-1-4">
						<div class="su-column-inner su-clearfix">
							<h2>
								<span class="light">Azienda Agricola</span>
								 Rapio
							</h2>
							<p>
								<strong>
								via Gramsci, 9
								<br>
								28079 Vespolate (NO)
								</strong>
							</p>
							<p>
								<strong>+39 0321 882227</strong>
								<br>
								<strong>
								<a href="mailto:info@risorapio.it">info@risorapio.it</a>
								</strong>
							</p>
						</div>
					</div>
					</div>
				</div>

				
			</article>
		</div>
  </div>
</div>
<?php } ?>

<?php get_footer(); ?>
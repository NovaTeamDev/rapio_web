<?php
/*
Template Name: ordina-online
*/
?>
<?php get_header(); 
wp_enqueue_style('jquery-style', 'http://www.risorapio.it/new/wp-content/plugins/shortcodes-ultimate/assets/css/box-shortcodes.css');
?>

<?php
if(isset($_POST['your-name'])) {
	echo $_POST['your-name'];
}
else {
?>

<script>
jQuery(document).ready(function () {
  jQuery(".numeric").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });
});

function verificaRiso() {
	var totale_riso = 0;
	totale_riso += jQuery('#qt1').val() * 1 ;
	totale_riso += jQuery('#qt3').val() * 1 ;
	totale_riso += jQuery('#qt4').val() * 1 ;
	totale_riso += jQuery('#qt5').val() * 1 ;
	totale_riso += jQuery('#qt6').val() * 1 ;
	totale_riso += jQuery('#qt7').val() * 1 ;

	jQuery('#riso1').empty();
	jQuery('#riso1').append(totale_riso);
}

function verificaFarina() {
	var totale_farina = 0;
	totale_farina += jQuery('#qt8').val();

	jQuery('#riso1').empty();
	jQuery('#riso1').append(totale_farina);
}

function calculate() {

	var totale = 0;
	totale += jQuery('#qt1').val() * 3;
	var tot1 = jQuery('#qt1').val() * 3;
	jQuery('#tot1').empty();
	jQuery('#tot1').append(tot1.toFixed(2) +" &euro;");
	
	totale += jQuery('#qt2').val() * 13.50 * 3;
	var tot2 = jQuery('#qt2').val() * 13.50 * 3;
	jQuery('#tot2').empty();
	jQuery('#tot2').append(tot2.toFixed(2) +" &euro;");
	
	totale += jQuery('#qt3').val() * 2.20;
	var tot3 = jQuery('#qt3').val() * 2.20;
	jQuery('#tot3').empty();
	jQuery('#tot3').append(tot3.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt4').val() * 2.20;
	var tot4 = jQuery('#qt4').val() * 2.20;
	jQuery('#tot4').empty();
	jQuery('#tot4').append(tot4.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt5').val() * 3;
	var tot5 = jQuery('#qt5').val() * 3;
	jQuery('#tot5').empty();
	jQuery('#tot5').append(tot5.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt6').val() * 3.50;
	var tot6 = jQuery('#qt6').val() * 3.50;
	jQuery('#tot6').empty();
	jQuery('#tot6').append(tot6.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt7').val() * 2.50;
	var tot7 = jQuery('#qt7').val() * 2.50;
	jQuery('#tot7').empty();
	jQuery('#tot7').append(tot7.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt8').val() * 1.20 * 20;
	var tot8 = jQuery('#qt8').val() * 1.20 * 20;
	jQuery('#tot8').empty();
	jQuery('#tot8').append(tot8.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt9').val() * 6 * 3;
	var tot9 = jQuery('#qt9').val() * 6 * 3;
	jQuery('#tot9').empty();
	jQuery('#tot9').append(tot9.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt10').val() * 6 * 3;
	var tot10 = jQuery('#qt10').val() * 6 * 3;
	jQuery('#tot10').empty();
	jQuery('#tot10').append(tot10.toFixed(2) +" &euro;");	
	
	totale += jQuery('#qt11').val() * 5 * 6;
	var tot11 = jQuery('#qt11').val() * 5 * 6;
	jQuery('#tot11').empty();
	jQuery('#tot11').append(tot11.toFixed(2) +" &euro;");	
	
	jQuery('#euro').empty();
	jQuery('#euro').append(totale.toFixed(2)+" &euro;");
}

function checkForm() {
	jQuery('#error_riso').empty();
	jQuery('#error_farina').empty();

	var remainder = jQuery('#riso1').text() % 10;
	var remainder2 = jQuery('#farina1').text() % 20;
	var check = true;
	
	if(remainder != 0) {
		jQuery('#error_riso').append('<font color="red">Le confezioni devono essere multipli di 10</font>');
		check =  false;
	}
	if(remainder2 != 0) {
		jQuery('#error_farina').append('<font color="red">Le confezioni devono essere multipli di 20</font>');
		check =  false;
	}	
	
return check;	


}
</script>

<div class="container push-down-60">
	<div class="row">
		<div class="col-xs-12" role="main">
			<article class="post-59 page type-page status-publish hentry">
				<header class="post-title center">
				<h1>Ordina Online</h1>
				<hr class="divider">
				<div class="text-shrink">
					<p class="text-highlight"> Qua Riccardo ci mette il testo </p>
				</div>
				<hr class="divider divider-about">
				</header>
				<div class="blog-content__text">
					<div class="su-row">
						<div class="su-column su-column-size-3-4">
							<div class="su-column-inner su-clearfix">
								<div id="wpcf7-f6-p59-o1" class="wpcf7" lang="it-IT" dir="ltr">
									<div class="screen-reader-response"></div>
									<form class="" method="post" action="" onsubmit="return checkForm()" name="">
										<div style="display: none;">
										</div>
												<p>
												<span class="hidden-xs">
													I campi contrassegnati con
													<span class="warning">*</span>
													sono obbligatori
												</span>
												</p>								
										<div class="row">
	
											<div class="col-xs-12 col-sm-6">
												<div class="form-group">
													<label class="text-dark" for="name">
														Nome e Cognome
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" type="text" aria-invalid="false" aria-required="true" size="40" value="" name="your-name">
													</span>
												</div>
												<div class="form-group">
													<label class="text-dark" for="cap">
														CAP
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" type="text" aria-invalid="false" aria-required="true" size="40" value="" name="your-email">
													</span>
												</div>
												<div class="form-group">
													<label class="text-dark" for="email">
													Email
													<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-email">
														<input class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" type="email" aria-invalid="false" aria-required="true" size="40" value="" name="your-email">
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="form-group">
													<label class="text-dark" for="name">
														Indirizzo
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" type="text" aria-invalid="false" aria-required="true" size="40" value="" name="your-address">
													</span>
												</div>
												<div class="form-group">
													<label class="text-dark" for="name">
														Comune
														<span class="warning">*</span>
													</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<input class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" type="text" aria-invalid="false" aria-required="true" size="40" value="" name="your-city">
													</span>
												</div>												
											</div>
											
											
											<div class="col-xs-12 col-sm">
												<div class="form-group">
													<table width="100%">
														<tr>
															<td width="10%"></td>
															<td width="40%">Prodotto</td>
															<td width="10%">Costo a confezione</td>
															<td width="10%">Quantit&agrave;</td>
															<td width="10%">Totale</td>
														</tr>
														<tr>
															<td width="10%" rowspan="2">
																<img src="../wp-content/uploads/2015/01/riso-carnaroli-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Riso Carnaroli 1 Kg sotto vuoto</td>
															<td width="20%">&euro; 3.00</td>
															<td width="20%"><input type="text" name="qt1" id="qt1" value="0" class="numeric" onkeyup="verificaRiso();calculate()" size="4" /></td>
															<td><div id="tot1">0.00 &euro;</div></td>
														</tr>
														<tr>
															<td width="20%">Riso Carnaroli 5 Kg sotto vuoto, cartone da 3 pz.</td>
															<td width="20%">&euro; 13.50</td>
															<td width="20%"><input type="text" name="qt2" id="qt2" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td><div id="tot2">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%">
																<img src="../wp-content/uploads/2015/01/riso-baldo-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Riso Baldo 1 Kg sotto vuoto</td>
															<td width="20%">&euro; 2.20</td>
															<td width="20%"><input type="text" name="qt3" id="qt3" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td><div id="tot3">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%">
																<img src="../wp-content/uploads/2015/01/riso-roma-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Riso Roma 1 Kg sotto vuoto</td>
															<td width="20%">&euro; 2.20</td>
															<td width="20%"><input type="text" name="qt4" id="qt4" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td><div id="tot4">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%">
																<img src="../wp-content/uploads/2015/01/riso-vialone-nano-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Riso Vialone Nano 1 Kg sotto vuoto</td>
															<td width="20%">&euro; 3.00</td>
															<td width="20%"><input type="text" name="qt5" id="qt5" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td><div id="tot5">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%">
																<img src="../wp-content/uploads/2015/01/riso-carnaroli-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Riso Carnaroli Extra 1 Kg sotto vuoto</td>
															<td width="20%">&euro; 3.50</td>
															<td width="20%"><input type="text" name="qt6" id="qt6" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td><div id="tot6">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%">
																<img src="../wp-content/uploads/2015/01/riso-integrale-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Riso Integrale 1 Kg sotto vuoto</td>
															<td width="20%">&euro; 2.50</td>
															<td width="20%"><input type="text" name="qt7" id="qt7" value="0" onkeyup="verificaRiso();calculate()" class="numeric" size="4" /></td>
															<td><div id="tot7">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%" rowspan="2">
																<img src="../wp-content/uploads/2015/01/farina-granoturco-bramata-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Farina di mais 500 gr. sotto vuoto</td>
															<td width="20%">&euro; 1.20</td>
															<td width="20%"><input type="text" name="qt8" id="qt8" value="0" onkeyup="verificaFarina();calculate()" class="numeric" size="4" /></td>
															<td><div id="tot8">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%">Farina di mais 4 kg sotto vuoto, cartone da 3pz.</td>
															<td width="20%">&euro; 6.00</td>
															<td width="20%"><input type="text" name="qt9" id="qt9" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td><div id="tot9">0.00 &euro;</div></td>
														</tr>
														<tr>
															<td width="20%">
																<img src="../wp-content/uploads/2015/01/farina-granoturco-bramata-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Farina di mais Integrale 4 kg sotto vuoto, cartone da 3pz.</td>
															<td width="20%">&euro; 6.00</td>
															<td width="20%"><input type="text" name="qt10" id="qt10" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td><div id="tot10">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%">
																<img src="../wp-content/uploads/2015/01/farina-granoturco-bramata-novara-264x300.jpg" width="80px" />
															</td>
															<td width="20%">Riso funghi 500 gr. , cartone da 6pz.</td>
															<td width="20%">&euro; 5.00</td>
															<td width="20%"><input type="text" name="qt11" id="qt11" value="0" onkeyup="calculate()" class="numeric" size="4" /></td>
															<td><div id="tot11">0.00 &euro;</div></td>
														</tr>	
														<tr>
															<td width="20%"></td>
															<td width="40%" colspan="2" align="center">Totale confezioni di riso da 1 Kg.</td>
															<td width="20%"><div id="riso1">0</div></td>
															<td></td>
														</tr>	
														<tr>
															<td width="20%"></td>
															<td width="40%" colspan="2" align="center"><div id="error_riso"></div></td>
															<td width="20%"></td>
															<td></td>
														</tr>															
														<tr>
															<td width="20%"></td>
															<td width="40%" colspan="2" align="center">Totale confezioni di farina da 500 gr.</td>
															<td width="20%"><div id="farina1">0</div></td>
															<td></td>
														</tr>	
														<tr>
															<td width="20%"></td>
															<td width="40%" colspan="2" align="center"><div id="error_farina"></div></td>
															<td width="20%"></td>
															<td></td>
														</tr>														
														<tr>
															<td width="20%"></td>
															<td width="40%" colspan="2" align="center">Totale</td>
															<td width="20%"></td>
															<td><div id="euro">0</div></td>
														</tr>															
													</table>
												</div>	
												<div class="col-xs-12 col-sm">
													<div class="form-group">
														<label class="text-dark" for="name">
															Metodo di pagamento
															<span class="warning">*</span>
														</label>
													<br>
													<span class="wpcf7-form-control-wrap your-name">
														<select>
															<option value=""></option>
															<option value="bonifico">Bonifico all'ordine</option>
															<option value="contrassegno">Contrassegno ( maggiorazione di 6 &euro; )</option>
														</select>
													</span>													
													</div>
												</div>
												<div class="right">
													<input class="wpcf7-form-control wpcf7-submit btn btn-warning" type="submit" value="INVIA">
												</div>
												<p></p>
											</div>											
											
											
										</div>
										<div class="wpcf7-response-output wpcf7-display-none"></div>
									</form>
								</div>
							</div>
						</div>
					<div class="su-column su-column-size-1-4">
						<div class="su-column-inner su-clearfix">
							<h2>
								<span class="light">Azienda Agricola</span>
								 Rapio
							</h2>
							<p>
								<strong>
								via Gramsci, 9
								<br>
								28079 Vespolate (NO)
								</strong>
							</p>
							<p>
								<strong>+39 0321 882227</strong>
								<br>
								<strong>
								<a href="mailto:info@risorapio.it">info@risorapio.it</a>
								</strong>
							</p>
						</div>
					</div>
					</div>
				</div>

				
			</article>
		</div>
	</div>
</div>
<?php } ?>

<?php get_footer(); ?>